package ocr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Point;

public class FeaturesVector implements Serializable {
    
	private static final long serialVersionUID = 1L;
	
	List<Point> points;
    List<Double> elements;
    public MatOfDouble elementsMat;
    double sizeFactor;
    
    private String className = "";
    boolean isPointsList;

    public FeaturesVector(List<Object> vector, boolean isPointsList) {
        if(isPointsList) {
            this.points = new ArrayList(vector);
        } else {
            this.elements = new ArrayList(vector);
//            this.elementsMat = convertToMat();
        }
        this.isPointsList = isPointsList;
    }
    
    public void setClassName(String className) {
        this.className = className;
    }
    
    public void setSizeFactor(double sizeFactor) {
        this.sizeFactor = sizeFactor;
    }
    
    public double getSizeFactorDistance(FeaturesVector featureVec) {
        return Math.abs(sizeFactor - featureVec.sizeFactor);
    }
    
    public double getDistance(FeaturesVector featureVec) {
        if(isPointsList) {
            return distancePointsVector(featureVec);
        } else {
            return distanceDoublesVector(featureVec);
        }
    }
    
    private double distancePointsVector(FeaturesVector vector) {
        List<Point> points1, points2;
        if(points.size() > vector.points.size()) {
            points1 = points; /* longer one */
            points2 = vector.points;
        } else {
            points1 = vector.points;
            points2 = points;
        }
        
        double sumDist = 0, tmpDist, minDist = Double.MAX_VALUE;
        for(Point p2 : points2) {
            for(Point p1 : points1) {
                tmpDist = pointDistance(p1, p2);
                if(tmpDist < minDist) {
                    minDist = tmpDist;
                }
            }
            sumDist += minDist;
        }
        
        sumDist = sumDist / points2.size();
        
        return sumDist;
    }
    
    private double distanceDoublesVector(FeaturesVector vector) {
        double sumDist = 0;
        
        for(int i = 0; i < elements.size(); i++) {
            sumDist += Math.abs(elements.get(i) - vector.elements.get(i));
        }
        
        return sumDist;
    }
    
    public MatOfDouble convertToMat() {
        MatOfDouble m = new MatOfDouble();
        m.fromList(elements);
        return m;
    }
    
    private double pointDistance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y- p1.y, 2));
    }
    
    @Override
    public String toString() {
        if(isPointsList) {
            return ""+ points;
        } else {
            return ""+ elements;
        }
        
    }

	public String getClassName() {
		return className;
	}
}
