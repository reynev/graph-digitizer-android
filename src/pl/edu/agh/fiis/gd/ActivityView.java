package pl.edu.agh.fiis.gd;

import static pl.edu.agh.fiis.gd.constants.Constants.TAG;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import pl.edu.agh.fiis.gd.constants.Constants;
import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.gd.view.PlotPropertiesDialog;
import pl.edu.agh.fiis.gd.view.RectView;
import pl.edu.agh.fiis.gd.view.RectView.ActionType;
import pl.edu.agh.fiis.gd.view.SpinnerColorsAdapter;
import pl.edu.agh.fiis.ocr.OcrProcess;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;


public class ActivityView extends Activity {
	
	private ImageView imgView;
	private Button applyBtn;
	private Bitmap plotBmp;
	private RectView rectView;
	private PlotPropertiesDialog.ComponentType editedTypeRect;
	private Spinner saveDataSpinner;
	private SpinnerColorsAdapter saveDataSpinnerAdapter;
	
	private HashMap<PlotPropertiesDialog.ComponentType, Rect> ocrRects = new HashMap<>();
	
	private final GraphDigitizerController gdc = GraphDigitizerController.getInstance();
	private AlertDialog propertiesDialog;
	
	private Uri fileUri;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gda);
        gdc.setAndroidView(this);
        gdc.initOcr();
        
        
        rectView = (RectView) findViewById(R.id.dragRect);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.graph_digitizer_android, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
        	case R.id.load_image:
        		invokeLoadFileIntent();
        	break;
        	case R.id.graph_properties:
        		showPropertiesDialog();
    		break;
        	case R.id.read_legend:
        		readLegend();
    		break;
        	case R.id.process_OCR:
        		processOCR();
//        		showError("Not implemented");
        	break;
        	case R.id.read_graph:
				try {
					gdc.process();
				} catch (GDAException e) {
					showError(e.getMessage());
				}
        	break;
        	case R.id.save_data:
        		showSaveDialog();
        	break;
        }
        if (id == R.id.load_image) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode == RESULT_OK){
            switch(requestCode){
                case Constants.REQUEST_SELECT_FILE:
                    fileUri = data.getData();
                    loadImageFromFilePath();
                    break;
                case Constants.REQUEST_SAVE_FILE:
                	Uri file = data.getData();
                	int plotNr = saveDataSpinner.getSelectedItemPosition();
                	gdc.saveCSV(new File(file.toString()), plotNr);
                	break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mOpenCVCallBack);
    }
    
    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
	   @Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				// Create and set View
				imgView = (ImageView) findViewById(R.id.imgView);
				applyBtn = (Button) findViewById(R.id.applyBtn);
				applyBtn.setOnClickListener(new ApplyClickListener());
				
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public void setImageFromBitmap(Bitmap image) {
		imgView.setImageBitmap(image);
	}
	
	public void setDescriptionRectangle(int x, int y, int width, int height,
			PlotPropertiesDialog.ComponentType type) {
		Rect rect = new Rect(x, y, x+width, y+height);
		ocrRects.put(type, rect);
	}
	
	public void editDescriptionRectangle(PlotPropertiesDialog.ComponentType type){
		propertiesDialog.dismiss();
		rectView.setStacicRect(ocrRects.get(type));
		System.err.println("EDITED RECT: " + ocrRects.get(type));
		rectView.enableDrawing(true, ActionType.SELECT_TEXT);
		applyBtn.setVisibility(View.VISIBLE);
		editedTypeRect = type;
	}

	public void showError(String string) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
			.setMessage(string)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		AlertDialog errorDialog = builder.create();
		errorDialog.show();
	}
	
	private void invokeLoadFileIntent(){
		 Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
         fileIntent.setType("image/*");
         startActivityForResult(fileIntent,Constants.REQUEST_SELECT_FILE);
	}
	
	/**
     * Loads image from file
     */
    private void loadImageFromFilePath(){
    	Cursor cursor = null;
    	String fileName = null;
    	Log.i("loadIMage","load image");
    	try { 
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = getContentResolver().query(fileUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			fileName = cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		if(fileName!= null){
			final String fN = fileName; 
			ocrRects.clear();
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			
			builder.setTitle("Photo")
			.setMessage("Is this file a photo or 'matlab' plot?")
			.setNegativeButton("Photo", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                	gdc.loadImage(fN,true);  
        			imgView.setImageBitmap(plotBmp);
        			rectView.countDisplayRatio(plotBmp.getWidth(),plotBmp.getHeight());
        			
        			dialog.dismiss();
                }
            })
            .setPositiveButton("Generated file", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                	gdc.loadImage(fN,false);  
        			imgView.setImageBitmap(plotBmp);
        			rectView.countDisplayRatio(plotBmp.getWidth(),plotBmp.getHeight());
        			
                    dialog.dismiss();
                }
            }).show();
		}
    }
    
    private void showPropertiesDialog(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

    	if(gdc.getPlotModel() != null){
    		final PlotPropertiesDialog ppd = new PlotPropertiesDialog(this,gdc.getPlotModel(),this);
    	
	        builder.setView(ppd)
	                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                        dialog.cancel();
	                    }
	                })
	                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog, int id) {
	                    	ppd.applyValues();
	                        dialog.dismiss();
	                    }
	                });
	        propertiesDialog = builder.create();
	        propertiesDialog.show();
    	}else{
    		showError("There is no valid plot");
    	}
    }
    
    private void showSaveDialog(){
    	if(gdc.getPlotModel() != null && gdc.getPlotModel() != null && !gdc.getPlotModel().getPlotValues().isEmpty()) {
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		LayoutInflater inflater = LayoutInflater.from(this);
    		View layout = inflater.inflate(R.layout.save_data_layout, null);
    		
    		builder.setTitle("Save plot data to csv");
    		builder.setView(layout);
    		saveDataSpinner = (Spinner) layout.findViewById(R.id.saveDataSpinner);
    		
    		/** TODO **/
    		saveDataSpinner.setAdapter(saveDataSpinnerAdapter); /* tu jest null pointer... **/
    		
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                	saveDataPickFilePath();
                    dialog.dismiss();
                }
            });
            AlertDialog saveDialog = builder.create();
            saveDialog.show();
    		
    	} else {
    		showError("Read graph first!");
    	}
    	
    }
    
    private void saveDataPickFilePath() {
    	Uri theUri = Uri.parse(fileUri.toString()).buildUpon().scheme("plot.csv").build();
        Intent theIntent = new Intent(Intent.ACTION_PICK);
        theIntent.setData(theUri);
        try {
            startActivityForResult(theIntent, Constants.REQUEST_SAVE_FILE);
        } catch (Exception e) {
//            e.printStackTrace();
//            showError("There is no activity ti saving files. Saving file in default location.");
            
            
			File folder = new File(Environment.getExternalStorageDirectory() + "/data");
			boolean success = true;
			if (!folder.exists()) {
			    success = folder.mkdir();
			}
            int plotNr = saveDataSpinner.getSelectedItemPosition();
        	gdc.saveCSV(new File(folder.getAbsolutePath()+"/plot" +plotNr+ "_dt_" + new Date().getTime() + ".csv"), plotNr);
        }
    }
    
    public void fillPlotSpinner(Map<Integer, Integer> plotsInLegend) {
		/*List<String> items = new ArrayList<>();
		for (Map.Entry<Integer, Integer> plot : plotsInLegend.entrySet()) {
            String item = "------- Plot "+ plot.getKey();
            items.add(item);
        }*/
    	saveDataSpinnerAdapter = new SpinnerColorsAdapter(this, plotsInLegend);
//		saveDataSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
	}
    
    private void readLegend(){
    	rectView.enableDrawing(true, ActionType.SELECT_LEGEND);
    	applyBtn.setVisibility(View.VISIBLE);
    }

	public void setPlotBmp(Bitmap plotBmp) {
		if(this.plotBmp != null){
			this.plotBmp.recycle();
		}
		this.plotBmp = plotBmp;
	}
	
	private class ApplyClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			Rect sR = rectView.getSelectedRect();
			if(sR != null){
				if(rectView.getActionType() == ActionType.SELECT_TEXT){
					ocrRects.put(editedTypeRect, sR);
				} else {
					gdc.handleLegend(sR.left, sR.top,
							sR.right-sR.left, sR.bottom - sR.top);
				}
			}
			rectView.disableSelecting();
			applyBtn.setVisibility(View.GONE);
		}
	}
	
	private void processOCR() {
		PlotPropertiesDialog.ComponentType propertyType;
        Rect rectangle;
        org.opencv.core.Rect rect;
        int labelOrientation;
        String value;
        
        for(Map.Entry<PlotPropertiesDialog.ComponentType, Rect> selectedArea : ocrRects.entrySet()) {
            rectangle = (Rect) selectedArea.getValue();
            rect = new org.opencv.core.Rect((int) (rectangle.left), (int) (rectangle.top),
                    (int) (rectangle.width()), (int) (rectangle.height()));
            propertyType = (PlotPropertiesDialog.ComponentType) selectedArea.getKey();
            
            labelOrientation = OcrProcess.HORIZONTAL;
            /** Text/number OCR **/
            if(propertyType.equals(PlotPropertiesDialog.ComponentType.TITLE) || propertyType.equals(PlotPropertiesDialog.ComponentType.X_TITLE) || 
            		propertyType.equals(PlotPropertiesDialog.ComponentType.Y_TITLE)) {
                if(propertyType.equals(PlotPropertiesDialog.ComponentType.Y_TITLE)) {
                    labelOrientation = OcrProcess.VERTICAL;
                }
                value = gdc.ocr.process(gdc.getPlotModel().getSourceImage().submat(rect), "all", labelOrientation);
            /** Only number OCR **/
            } else { 
                /** Vertical orientation if Y **/
                if(propertyType.equals(PlotPropertiesDialog.ComponentType.Y_MIN) || propertyType.equals(PlotPropertiesDialog.ComponentType.Y_MAX)) {
                    labelOrientation = OcrProcess.VERTICAL;
                }
                value = gdc.ocr.process(gdc.getPlotModel().getSourceImage().submat(rect), "number", labelOrientation);
            }
            switch (propertyType) {
			case TITLE:
				gdc.getPlotModel().setTitle(value);
				break;
			case X_TITLE:
				gdc.getPlotModel().setxTitle(value);
				break;
			case Y_TITLE:
				gdc.getPlotModel().setyTitle(value);
				break;
			case X_MIN:
				gdc.getPlotModel().setxMin(value);
				break;
			case X_MAX:
				gdc.getPlotModel().setxMax(value);
				break;
			case Y_MIN:
				gdc.getPlotModel().setyMin(value);
				break;
			case Y_MAX:
				gdc.getPlotModel().setyMax(value);
				break;
			default:
				break;
			}
        }
    }
	
}
