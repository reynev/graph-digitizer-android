package pl.edu.agh.fiis.gd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.gd.model.PlotModel;
import pl.edu.agh.fiis.gd.model.PlotModel.PlotType;
import pl.edu.agh.fiis.gd.preprocessing.Preprocessing;
import pl.edu.agh.fiis.gd.processes.ColourSegmentation;
import pl.edu.agh.fiis.gd.processes.DescriptionDetector;
import pl.edu.agh.fiis.gd.processes.PlotAreaDetectorProcess;
import pl.edu.agh.fiis.gd.processes.PlotValuesReaderProcess;
import pl.edu.agh.fiis.gd.processes.ScaleConverter;
import pl.edu.agh.fiis.gd.processes.TicksDetectorProcess;
import pl.edu.agh.fiis.gd.view.PlotPropertiesDialog.ComponentType;
import pl.edu.agh.fiis.ocr.OcrProcess;
import pl.edu.agh.fiis.utils.ImageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;

/**
 * Main class
 *
 * @author Drobny, reynev
 */
public class GraphDigitizerController {

    private ActivityView androidView;

//    private Mat resultImage;
//    private Bitmap sourceImageBuffered;
    private PlotModel plotModel;
    public OcrProcess ocr;

    private static volatile GraphDigitizerController instance = null;
    
    private GraphDigitizerController(){};
    
    public static GraphDigitizerController getInstance(){
    	if (instance == null) {
            synchronized (GraphDigitizerController.class) {
                if (instance == null) {
                    instance = new GraphDigitizerController();
                }
            }
        }
        return instance;
    }
    
    public void initOcr() {
    	ocr = new OcrProcess(androidView.getApplicationContext()); 
    }

    /**
     * Loads image from file and creates new plot model
     *
     * @param fileName
     * @throws GDAException 
     */
    public void loadImage(String fileName, boolean isPhoto) {
        Mat si = ImageManager.loadImageMat(fileName);
//        Mat ri = new Mat();
//        Imgproc.resize(si, ri, new Size(2*si.width(),2*si.height()));
        
        Log.d("image",""+ si.size()); 
        plotModel = new PlotModel(si);
        Mat siCp = si;// new Mat();

//        Imgproc.cvtColor(si, siCp, Imgproc.COLOR_BGR2RGB);
        Bitmap sourceBitmap = Bitmap.createBitmap(si.width(), si.height(),
        		Config.ARGB_8888);
		Utils.matToBitmap(siCp, sourceBitmap);
        androidView.setPlotBmp(sourceBitmap);
        
        if(isPhoto){
        	Preprocessing.process(plotModel);
            plotModel.setProcessing(true);
        }
		
        findIntrestingAreasOnImage();
    }

    /**
     * Reading points from plot
     */
    public void process() throws GDAException {
    	System.err.println("READ START");
    	
//        if (plotModel.isHasLegend()&&!plotModel.isLegendReaded())
//            throw new GDAException("If plot has legend read it.");

        /**
         * Process starts here *
         */
//        resultImage=new Mat();

        Rect areaRect=plotModel.getAreaRect();
        Rect plotRect=new Rect(new Point(areaRect.x+2,areaRect.y+2),
                               new Point(areaRect.x+areaRect.width-2,areaRect.y+areaRect.height-2));

        plotModel.setAreaRect(plotRect);
        PlotValuesReaderProcess.process(plotModel);

        savePlotPoints(areaRect);

        double xMin = Double.parseDouble(plotModel.getxMin());
        double xMax = Double.parseDouble(plotModel.getxMax());
        double yMin = Double.parseDouble(plotModel.getyMin());
        double yMax = Double.parseDouble(plotModel.getyMax());

        ScaleConverter.countCoefficients(plotModel,xMin,xMax,yMin,yMax);

        androidView.fillPlotSpinner(plotModel.getPlotsInLegend());
        System.err.println("READ END");
    }

    /**
     * Preprocessing for reading plot: - detects plot area - detects ticks -
     * detects rectangles with axis (x,y) values (min/max) and titles
     */
    public void findIntrestingAreasOnImage() {

//        resultImage=new Mat();
        PlotAreaDetectorProcess.process(plotModel);

        TicksDetectorProcess.process(plotModel);

        List<Point> xTicksList=plotModel.getxTicksList();
        List<Point> yTicksList=plotModel.getyTicksList();

        int xListS=xTicksList.size();
        int yListS=yTicksList.size();
        double[] xLoc=new double[xListS];
        double[] yLoc=new double[yListS];

        for (int i=0; i<xListS; ++i)
            xLoc[i]=xTicksList.get(i).x;
        for (int i=0; i<yListS; ++i)
            yLoc[i]=yTicksList.get(i).y;
        Arrays.sort(xLoc);
        Arrays.sort(yLoc);
        Rect plotAreaRect=plotModel.getAreaRect();
        DescriptionDetector descDetector=new DescriptionDetector(plotModel.getSourceImage());
        Rect xminR=null, xmaxR=null, yminR=null, ymaxR=null;
        try {
            Point xmin=new Point(xLoc[0]+plotAreaRect.x,plotAreaRect.y+plotAreaRect.height);
            Point xmax=new Point(xLoc[xListS-1]+plotAreaRect.x,plotAreaRect.y+plotAreaRect.height);
            Point ymin=new Point(0+plotAreaRect.x,yLoc[yListS-1]+plotAreaRect.y);
            Point ymax=new Point(0+plotAreaRect.x,yLoc[0]+plotAreaRect.y);

            xminR=descDetector.detectScaleDescription(
                    xmin,DescriptionDetector.SCALE_X);
            xmaxR=descDetector.detectScaleDescription(
                    xmax,DescriptionDetector.SCALE_X);
            yminR=descDetector.detectScaleDescription(
                    ymin,DescriptionDetector.SCALE_Y);
            ymaxR=descDetector.detectScaleDescription(
                    ymax,DescriptionDetector.SCALE_Y);
            androidView.setDescriptionRectangle(xminR.x,xminR.y,xminR.width,xminR.height, 
            		ComponentType.X_MIN);
            androidView.setDescriptionRectangle(xmaxR.x,xmaxR.y,xmaxR.width,xmaxR.height,
            		ComponentType.X_MAX);
            androidView.setDescriptionRectangle(yminR.x,yminR.y,yminR.width,yminR.height,
            		ComponentType.Y_MIN);
            androidView.setDescriptionRectangle(ymaxR.x,ymaxR.y,ymaxR.width,ymaxR.height,
            		ComponentType.Y_MAX);

        }
        catch (ArrayIndexOutOfBoundsException e) {
            androidView.showError("Problem with reading scale ticks.");
        }

        try {
            Point xAxisCenter=new Point((xminR.x+xminR.width/2+xmaxR.x+xmaxR.width/2)/2,
                                        xminR.y+xminR.height);
            Rect xTitleR=descDetector.detectScaleDescription(
                    xAxisCenter,DescriptionDetector.SCALE_X);
            if (xTitleR!=null)
                androidView.setDescriptionRectangle(xTitleR.x,xTitleR.y,xTitleR.width,xTitleR.height,
                		ComponentType.X_TITLE);
        }
        catch (NullPointerException e) {
            System.err.println("X rectangles missing");
        }
        try {
            Point yAxisCenter=new Point(yminR.x,
                                        (yminR.y+yminR.height/2+ymaxR.y+ymaxR.height/2)/2);
            Rect yTitleR=descDetector.detectScaleDescription(
                    yAxisCenter,DescriptionDetector.SCALE_Y);
            if (yTitleR!=null)
                androidView.setDescriptionRectangle(yTitleR.x,yTitleR.y,yTitleR.width,yTitleR.height,
                		ComponentType.Y_TITLE);
        }
        catch (NullPointerException e) {
            System.err.println("Y rectangles missing");
        }

    }

    public Map<Double,String> handleLegend(int x,int y,int w,int h) {
        Map<Double,String> colourPlots=new HashMap<>();
//        Rect roi=new Rect(x,y,w,h);
//        Mat subImg=new Mat(plotModel.getSourceImage(),roi);
//        ArrayList<Double> peaks=ColourSegmentation.process(subImg);
        plotModel.getPlotsInLegend().clear();
//        for (Double d:peaks) {
//        	float[] hsv = {(float)(d/256.0f),0.7f, 1};
//            plotModel.getPlotsInLegend().put(peaks.indexOf(d),Color.HSVToColor(hsv));
//            colourPlots.put(d,"Plot "+d+":");
//        }
        removeAreaOnImage(x,y,w,h);
//        System.err.println("Peaks in legend: " + peaks);
        ImageManager.saveImageMat("resources/legend.png", plotModel.getSourceImage());
        plotModel.setLegendReaded(true);
        
        return colourPlots;
    }

    public void saveCSV(File file,int i) {

        Map<Integer,List<Point>> graphsPoints=ScaleConverter.countGraphPoints(plotModel);

        try {
            PrintWriter pw=new PrintWriter(file);
            StringBuilder dataFileContent=new StringBuilder();
            for (Point gp:graphsPoints.get(i)) {
                dataFileContent.append("\n");
                dataFileContent.append(gp.x);
                dataFileContent.append(",");
                dataFileContent.append(gp.y);
            }

            pw.print(dataFileContent.toString());
            pw.close();
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(GraphDigitizerController.class.getName()).log(Level.SEVERE,null,ex);
        }
    }

    /**
     * Debug function
     */
    private void savePlotPoints(Rect areaRect) {
        Bitmap tempImage = Bitmap.createBitmap(areaRect.width,areaRect.height,Bitmap.Config.ARGB_8888);
        for (int i=0; i<areaRect.width; i++)
            for (int j=0; j<areaRect.height; j++)
                tempImage.setPixel(i,j,Color.WHITE);

        Map<Integer,List<Point>> plotValues=plotModel.getPlotValues();
        Canvas cvs = new Canvas(tempImage);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Style.FILL);
        for (Integer c:plotValues.keySet())
            if (plotValues.get(c).size()>2) {
//                System.out.print("Color(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + "): ");
                for (Point p:plotValues.get(c)) {
                    if (p!=null) {
//                        System.out.print("(" + p.x + "," + p.y + "), ");
                    }
                    if(plotModel.getType() == PlotType.TYPE_LINE){
                        tempImage.setPixel((int)p.x,(int)p.y,Color.BLACK);
                    }else{
                    	cvs.drawRect((int)p.x-1,(int)p.y-1,(int)p.x+1,(int)p.y+1, paint);
                    }
                }
                System.out.print("\n");
            }
        Mat tmpMat = new Mat();
        Utils.bitmapToMat(tempImage, tmpMat);
        ImageManager.saveImageMat("resources/saved.png", tmpMat);
    }

    /**
     * Removes part of image without displaing changes
     *
     * @param x
     * @param y
     * @param w
     * @param h
     */
    public void removeAreaOnImage(int x,int y,int w,int h) {
        Core.rectangle(plotModel.getSourceImage(),new Point(x,y),
                       new Point(x+w,y+h),new Scalar(255,255,255),Core.FILLED);
//        sourceImageBuffered = ImageManager.mat2Img(sourceImage);
//        controller.setImageFromBuffered(sourceImageBuffered);
    }

	public void setAndroidView(ActivityView androidView) {
		this.androidView = androidView;
	}

	public PlotModel getPlotModel() {
		return plotModel;
	}
	
	
}

