package pl.edu.agh.fiis.gd.constants;

public class Constants {

	public static final String TAG = "OPEN_CV";
	public static final int REQUEST_SELECT_FILE = 101;
	public static final int REQUEST_SAVE_FILE = 201;
}
