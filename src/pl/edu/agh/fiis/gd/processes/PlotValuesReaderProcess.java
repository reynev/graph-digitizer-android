/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.fiis.gd.processes;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.gd.model.PlotModel;
import pl.edu.agh.fiis.gd.model.PlotModel.PlotType;
import android.graphics.Color;

/*
 * Things to change:
 * - Reading if it is line or point graph from GUI
 * - Reading point values
 * - Reading values from pure black line graph (it's not finished)
 * 
 * #########################################################################################
 *   IF YOU WANT TO SEE JPG WITH EACH COMPUTED GRAPH CHANGE VALUE OF "SAVE_MATS" TO TRUE
 * #########################################################################################
 */
public class PlotValuesReaderProcess {

    private static double REJECTION_VALUE=0.05;
    private static int LONG_LINE = 30;
    private static int ACCEPTABLE_DISTANCE=10;
    private static float LUMOSITY_THRESHOLD=257.0f;
    private static boolean SAVE_MATS=true;
    // for each isolated graph saves two files: 
    // "XXX plot" for single graph or "XXX plot Y" for multiple - it's final result of my class
    // "pre_XXX plot" or "pre_XXX plot Y" - these files shows how the curves looks before choosing only one pixel per column

    public static void process(PlotModel plotModel) {

        ArrayList<Double> confirmed_hues=new ArrayList<Double>();
        Map<Integer,Integer> plotsInLegend = plotModel.getPlotsInLegend();
        Map<Integer,List<Point>> plotValues=new HashMap<Integer,List<Point>>();

        Mat sourceImage=new Mat(plotModel.getSourceImage(),plotModel.getAreaRect());
        Mat hsvImage=sourceImage.clone();
        Imgproc.cvtColor(hsvImage,hsvImage,Imgproc.COLOR_RGB2HSV);

        //Compute confirmed_hues 
        if (plotsInLegend.isEmpty()) {
            ArrayList<Double> peaks=ColourSegmentation.process(sourceImage); // find hues peaks
            for (Double d:peaks)
                confirmed_hues.add(d);
        }
        else {
            confirmed_hues=new ArrayList<Double>();
            for (Integer d:plotsInLegend.keySet()) {
                int c=plotsInLegend.get(d);
                float[] vals = new float[3];
                Color.colorToHSV(c, vals);
                confirmed_hues.add((double)(int)(vals[0]*256));

            }
        }
        if (confirmed_hues.size()==0)
            new GDAException("Can't isolate any colours from source image.");

        plotsInLegend.clear();


        /* 
         --------------- Black colour -------------
         */
        Mat singleColourMat=sourceImage.clone();
        for (int i=0; i<singleColourMat.width(); i++)
            for (int j=0; j<singleColourMat.height(); j++) {
                int value=(int)((sourceImage.get(j,i)[0]+sourceImage.get(j,i)[1]+sourceImage.get(j,i)[2])/3);
                if (Math.abs(sourceImage.get(j,i)[0]-value)+Math.abs(sourceImage.get(j,i)[1]-value)+Math.abs(sourceImage.get(j,i)[2]-value)<40)
                    Core.rectangle(sourceImage,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED); // remove pixel from sourceImage
                else
                    Core.rectangle(singleColourMat,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED); // remove Pixel from singleColourMat
            }

        Imgproc.cvtColor(singleColourMat,singleColourMat,Imgproc.COLOR_RGB2GRAY);
        Mat thresholdedMat=new Mat(singleColourMat.rows(),singleColourMat.cols(),singleColourMat.channels());
        Imgproc.threshold(singleColourMat,thresholdedMat,200,255,Imgproc.THRESH_BINARY); // Threshold(200)
        Imgproc.cvtColor(thresholdedMat,thresholdedMat,Imgproc.COLOR_GRAY2RGB); // convert back to RGB
        removeScale(thresholdedMat,plotModel);

        saveMat(thresholdedMat,"step1.png");

        if (plotModel.getType() == PlotType.TYPE_LINE) {

            Mat thinnedMat=thinning(thresholdedMat);
            if (countPoints(thinnedMat)>100) { // compute if there are more than 100 gray points on image

                boolean multiple_lines=false, long_line=false, too_many_shapes=false;

                // this loop checks if there are more than one graph on Mat and if the curve is a function
                for (int i=0; i<thinnedMat.width(); i++) {
                    int distance=0, shapes=0, start_y=-1, end_y=0;
                    boolean line_present=false;

                    for (int j=0; j<thinnedMat.height(); j++)
                        if (thinnedMat.get(j,i)[0]==255) {
                            distance++;
                            if (start_y!=-1) {
                                if (end_y-start_y+1>LONG_LINE)
                                    long_line=true;
                                shapes++;
                                start_y=-1;
                            }
                        }
                        else {
                            if (start_y==-1)
                                start_y=j;
                            end_y=j;
                            if (line_present&&distance>ACCEPTABLE_DISTANCE)
                                multiple_lines=true;
                            line_present=true;
                            distance=0;
                        }
                    // TO_CHANGE
                    if (shapes>10)
                        too_many_shapes=true;
                }

                // if there are more than one graph
                if (multiple_lines) {
                    System.err.println("Multiple black graph");
                    if (!long_line&&!too_many_shapes) {

                        ArrayList<Mat> mats=isolateGraphs(thinnedMat);

                        for (Mat m:mats) {
                            ArrayList<Point> points=curveToFunction(m);
                            System.err.println("\tPlot #"+mats.indexOf(m)+": "+points.size()+" points found.");
                            plotValues.put(plotValues.size(),points);
                            plotsInLegend.put(plotsInLegend.size(),0);
                            if (SAVE_MATS)
                                saveMat(printPoints(points,m.width(),m.height(),m.type()),"end_results"+File.separator+"black plot "+mats.indexOf(m));
                        }
                    }
                    else
                        new GDAException("\tBlack points cannot be read because they are not a function");
                    System.err.println("\tBlack points cannot be read because they are not a function");
                }
                else {

                    ArrayList<Point> points=curveToFunction(thinnedMat);

                    if (points.size()>10) {

                        System.err.println("Single black graph");
                        System.err.println("\t"+points.size()+" points found");
                        plotValues.put(plotValues.size(),points);
                        plotsInLegend.put(plotsInLegend.size(),0);
                        if (SAVE_MATS)
                            saveMat(printPoints(points,thinnedMat.width(),thinnedMat.height(),thinnedMat.type()),"end_results"+File.separator+"black plot");
                    }
                }
            }
        }
        else if (plotModel.getType()==PlotType.TYPE_POINT) {
            ArrayList<Point> result=handlePointGraph(thresholdedMat,-1);

            if (result.size()>5) {
                System.err.println("Black graph\t"+result.size()+" points found");
                plotValues.put(plotValues.size(),result);
                plotsInLegend.put(plotsInLegend.size(),0);
                if (SAVE_MATS)
                    saveMat(printPoints(result,thresholdedMat.width(),thresholdedMat.height(),thresholdedMat.type()),"end_results"+File.separator+"black plot");
            }
        }

        for (Double f:confirmed_hues) {
            // Isolating hue == f
            Mat line_image=sourceImage.clone(), thinned=new Mat();
            int iterator=0;
            for (int i=0; i<line_image.width(); i++)
                for (int j=0; j<line_image.height(); j++)
                    if (Math.abs(hsvImage.get(j,i)[0]-f)>5)
                        Core.rectangle(line_image,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED);
                    else {
                        Core.rectangle(hsvImage,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED);
                        iterator++;
                    }
            if (plotModel.getType() == PlotType.TYPE_LINE) {
                if (iterator<50)
                    continue;
                // Thinning
                thinned=thinning(line_image);

                // Checking if there are more than one graph with specific hue         
                boolean multiple_lines=false;
                boolean long_line=false;
                boolean too_many_shapes=false;
                for (int i=0; i<thinned.width(); i++) {
                    int shapes=0;
                    int distance=0;
                    boolean line_present=false;
                    int start_y=-1, end_y=0;
                    for (int j=0; j<thinned.height(); j++)

                        if (thinned.get(j,i)[0]==255) {
                            distance++;
                            if (start_y!=-1) {
                                if (end_y-start_y+1>LONG_LINE)
                                    long_line=true;
                                shapes++;
                                start_y=-1;
                            }
                        }
                        else {
                            if (start_y==-1)
                                start_y=j;
                            end_y=j;
                            if (line_present&&distance>ACCEPTABLE_DISTANCE)
                                multiple_lines=true;
                            line_present=true;
                            distance=0;
                        }
                    if (shapes>10)
                        too_many_shapes=true;
                }

                // If there is only one graph
                if (long_line||too_many_shapes)
                    System.err.println("\tGraph (hue= "+f+") cannot be read, because it is not a function");
                else {
                    if (!multiple_lines) {

                        ArrayList<Point> points=curveToFunction(thinned);
                        if (points.size()>10) {
                            System.err.print("Single graph. Hue= "+f);
                            System.err.println(". \n\tPoints found: "+points.size());
                            plotValues.put(plotValues.size(),points);
                            float[] hsv = {f.floatValue()*2,0.7f, 1};
                            plotsInLegend.put(plotsInLegend.size(),Color.HSVToColor(hsv));
                            if (SAVE_MATS)
                                saveMat(printPoints(points,thinned.width(),thinned.height(),thinned.type()),"end_results"+File.separator+f+" plot");
                        }
                    }
                    if (multiple_lines) {
                        System.err.println("Multiple graph. Hue= "+f);
                        ArrayList<Mat> mats=isolateGraphs(thinned);
                        for (Mat m:mats) {
                            ArrayList<Point> points=curveToFunction(m);
                            System.err.println("\tPlot #"+mats.indexOf(m)+": "+points.size()+" points found.");
                            plotValues.put(plotValues.size(),points);
                            float[] hsv = {f.floatValue()*2,0.7f, 1};
                            plotsInLegend.put(plotsInLegend.size(),Color.HSVToColor(hsv));
                            if (SAVE_MATS)
                                saveMat(printPoints(points,m.width(),m.height(),m.type()),"end_results"+File.separator+f.toString()+" plot "+mats.indexOf(m));
                        }
                    }
                }
            }
            else if (plotModel.getType()==PlotType.TYPE_POINT) {
                for (int i=0; i<line_image.width(); i++)
                    for (int j=0; j<line_image.height(); j++)
                        if (line_image.get(j,i)[0]!=255)
                            Core.line(line_image,new Point(i,j),new Point(i,j),new Scalar(0,0,0));

                ArrayList<Point> result=handlePointGraph(line_image,f);
                System.err.println("Hue="+f.toString()+"\n\t"+result.size()+" points found");
                plotValues.put(plotValues.size(),result);
                float[] hsv = {f.floatValue()*2,0.7f, 1};
                plotsInLegend.put(plotsInLegend.size(),Color.HSVToColor(hsv));
                if (SAVE_MATS)
                    saveMat(printPoints(result,thresholdedMat.width(),thresholdedMat.height(),thresholdedMat.type()),"end_results"+File.separator+f.toString()+" plot");
            }
        }
        plotModel.setPlotsInLegend(plotsInLegend);
        plotModel.setPlotValues(plotValues);
        System.err.println("Done!");

    }

    private static ArrayList<Point> handlePointGraph(Mat thresholdedMat,double d) {

        File step4=new File("resources/end_results");
        if (!step4.exists())
            step4.mkdirs();

        ArrayList<ArrayList<Point>> shapes=isolateShapes(thresholdedMat);
        for (int i=0; i<shapes.size(); i++)
            if (shapes.get(i).size()<4) {
                for (Point p:shapes.get(i))
                    Core.line(thresholdedMat,p,p,new Scalar(255,255,255));
                shapes.remove(i);
                i--;
            }

        Map<Integer,Integer> shapeHistogram=new HashMap<Integer,Integer>();
        for (ArrayList<Point> shape:shapes){
            if (shapeHistogram.containsKey(shape.size())){
                shapeHistogram.put(shape.size(),shapeHistogram.get(shape.size())+1);
            }else{
                shapeHistogram.put(shape.size(),1);
            }
        }

        int popular=0;
        for (Integer i:shapeHistogram.keySet()) {
            if (popular==0)
                popular=i;
            if (shapeHistogram.get(popular)<shapeHistogram.get(i))
                popular=i;
        }
        ArrayList<Point> smallest=new ArrayList<Point>();

        // System.err.println(popular);
        for (ArrayList<Point> shape:shapes)
            if (shape.size()==popular) {
                smallest=shape;
                break;
            }

//       
//        for (int i=0; i<shapes.size(); i++)
//            if (smallest.size()==0)
//                smallest=shapes.get(i);
//            else if (smallest.size()>shapes.get(i).size())
//                smallest=shapes.get(i);
        double left=-1, right=-1, top=-1, bottom=-1;
        for (Point p:smallest) {
            if (left==-1) {
                left=p.x;
                right=p.x;
                top=p.y;
                bottom=p.y;
            }
            if (left>p.x)
                left=p.x;
            if (right<p.x)
                right=p.x;
            if (top>p.y)
                top=p.y;
            if (bottom<p.y)
                bottom=p.y;
        }

        Mat point=new Mat((int)(bottom-top+1),(int)(right-left+1),thresholdedMat.type());
        Core.rectangle(point,new Point(0,0),new Point(point.width()-1,point.height()-1),new Scalar(255,255,255),Core.FILLED);

        for (Point p:smallest) {
            Core.line(point,new Point(p.x-left,p.y-top),new Point(p.x-left,p.y-top),new Scalar(0,0,0));
            Core.line(thresholdedMat,p,p,new Scalar(0,0,255));
        }
        if (smallest.size()>50)
            System.err.println("Can't find isolated shape");
        ArrayList<Point> result=findPoints(thresholdedMat,point,d);
        return result;
    }

    private static ArrayList<Point> findPoints(Mat ref,Mat tmpl,double d) {

        // / Create the result matrix
        int match_method=Imgproc.TM_CCOEFF;
        int result_cols=ref.cols()-tmpl.cols()+1;
        int result_rows=ref.rows()-tmpl.rows()+1;
        Mat result=null;
        // / Do the Matching and Normalize
        ArrayList<Point> points=new ArrayList<Point>();
        int main_it=0;
        double first=Double.MIN_VALUE;
        while (main_it<10) {
            result=new Mat(result_rows,result_cols,ref.type());
            main_it++;

            ArrayList<Point> temp_points=new ArrayList<Point>();
            Imgproc.matchTemplate(ref,tmpl,result,match_method);

            if (main_it==1)
                for (int i=0; i<result.width(); i++)
                    for (int j=0; j<result.height(); j++)
                        if (result.get(j,i)[0]>first)
                            first=result.get(j,i)[0];

            double current=Double.MIN_VALUE;
            for (int i=0; i<result.width(); i++)
                for (int j=0; j<result.height(); j++)
                    if (result.get(j,i)[0]>current)
                        current=result.get(j,i)[0];

            Core.normalize(result,result,0,256,Core.NORM_MINMAX,-1,new Mat());
            //     saveMat(result,"step2"+File.separator+"iteration"+main_it+File.separator+d);
            Imgproc.threshold(result,result,240,255,Imgproc.THRESH_TOZERO);

            for (int i=0; i<result.width(); i++)
                for (int j=0; j<result.height(); j++)
                    if (result.get(j,i)[0]!=0)
                        temp_points.add(new Point(i,j));
            if (temp_points.size()>300||temp_points.size()==0)
                break;

            if (current<=first*1/3)
                break;

            for (Point p:temp_points)
                for (int i=0; i<tmpl.width(); i++)
                    for (int j=0; j<tmpl.height(); j++)
                        if (tmpl.get(j,i)[0]==0)
                            Core.line(ref,new Point(p.x+i,p.y+j),new Point(p.x+i,p.y+j),new Scalar(255,255,255));
            points.addAll(temp_points);

            //   saveMat(result,"step3"+File.separator+"iteration"+main_it+File.separator+d);
        }
        return points;
    }

    private static ArrayList<Mat> isolateGraphs(Mat mat) {
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_GRAY2BGR);
        Mat mat2=mat.clone();

        for (int i=0; i<mat.width(); i++)
            for (int j=0; j<mat.height(); j++)
                if (!equal(mat.get(j,i),255,255,255)) {
                    try {
                        int it_f=0;
                        for (int a=-2; a<3; a++)
                            for (int b=-2; b<3; b++)
                                if (!(a==b&&b==0))
                                    if (!equal(mat.get(j+a,i+b),255,255,255))
                                        it_f++;
                        if (it_f>5) {
                            Core.rectangle(mat,new Point(i-2,j-2),new Point(i+2,j+2),new Scalar(255,255,255),Core.FILLED);
                            continue;
                        }
                    }
                    catch (Exception e) {
                    }
                    try {
                        if (!equal(mat.get(j+1,i),255,255,255)||!equal(mat.get(j-1,i),255,255,255))
                            Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(255,255,0),Core.FILLED);
                        if (!equal(mat.get(j,i+1),255,255,255)||!equal(mat.get(j,i-1),255,255,255))
                            Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(0,255,0),Core.FILLED);

                        if (!equal(mat.get(j+1,i+1),255,255,255)||!equal(mat.get(j-1,i-1),255,255,255))
                            Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(255,0,0),Core.FILLED);

                        if (!equal(mat.get(j-1,i+1),255,255,255)||!equal(mat.get(j+1,i-1),255,255,255))
                            Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(0,0,255),Core.FILLED);
                    }
                    catch (Exception e) {
                    }
                }
        ArrayList<ArrayList<Point>> shapes=isolateShapes(mat);
        ArrayList<ArrayList<Point>> shape_ends=new ArrayList<ArrayList<Point>>();

        for (ArrayList<Point> v:shapes) {
            ArrayList<Point> ends=new ArrayList<Point>();
            ends.add(v.get(0));
            ends.add(v.get(v.size()-1));
            if (ends.size()!=0)
                shape_ends.add(ends);
        }

        ArrayList<Link> links=new ArrayList<Link>();

        for (ArrayList<Point> v:shape_ends)
            for (Point p1:v)
                for (ArrayList<Point> v2:shape_ends) {
                    double a=v.get(0).x;
                    double b=v.get(v.size()-1).x;
                    double c=v2.get(0).x;
                    double d=v2.get(v2.size()-1).x;
                    if ((c<a&&d<a)||(c>b&&d>b))

                        if (v2!=v)
                            for (Point p2:v2)
                                if (Math.hypot(p1.x-p2.x,p1.y-p2.y)<100)
                                    links.add(new Link(p1,p2,shape_ends.indexOf(v),shape_ends.indexOf(v2)));
                }
        for (Link l:links)
            for (Link l2:links)
                if (l2!=l&&l.isValid()&&l2.isValid()) {
                    if ((l.getA()==l2.getB())&&(l2.getA()==l.getB()))
                        l2.setValid(false);
                    if ((l2.shapeA()==l.shapeA()&&l2.shapeB()==l.shapeB())
                            ||(l2.shapeA()==l.shapeB()&&l2.shapeB()==l.shapeA()))
                        if (l.getDistance()>l2.getDistance())
                            l.setValid(false);
                        else
                            l2.setValid(false);
                }
        links=clearLinks(links);
        for (Link l:links)
            for (Link l2:links)
                if (l2!=l&&l.isValid()&&l2.isValid())
                    if ((l2.shapeA()==l.shapeA())||(l2.shapeB()==l.shapeB())||(l2.shapeA()==l.shapeB())||(l2.shapeB()==l.shapeA())) {
                        if (l.getDistance()>4*l2.getDistance())
                            l.setValid(false);
                        if (l2.getDistance()>4*l2.getDistance())
                            l2.setValid(false);
                    }
        links=clearLinks(links);
        for (Link l:links)
            if (l.isValid()) {
                List<Point> v1=shapes.get(l.indexA);
                List<Point> v2=shapes.get(l.indexB);

                ArrayList<Integer> p1=new ArrayList<Integer>();
                ArrayList<Integer> p2=new ArrayList<Integer>();

                int size1=v1.size()>10?10:v1.size()-1;
                int size2=v2.size()>10?10:v2.size()-1;

                if (v1.indexOf(l.getA())==0)
                    v1=v1.subList(0,size1);
                else {
                    v1=v1.subList(v1.size()-1-size1,v1.size()-1);
                    Collections.reverse(v1);
                }

                if (v2.indexOf(l.getB())==0)
                    v2=v2.subList(0,size2);
                else {
                    v2=v2.subList(v2.size()-1-size2,v2.size()-1);
                    Collections.reverse(v2);
                }

                for (Point p:v1) {
                    if (equal(mat.get((int)p.y,(int)p.x),0,0,255))
                        p1.add(1);
                    if (equal(mat.get((int)p.y,(int)p.x),255,255,0))
                        p1.add(0);
                    if (equal(mat.get((int)p.y,(int)p.x),0,255,0))
                        p1.add(2);
                    if (equal(mat.get((int)p.y,(int)p.x),255,0,0))
                        p1.add(3);
                }

                for (Point p:v2) {
                    if (equal(mat.get((int)p.y,(int)p.x),0,0,255))
                        p2.add(1);
                    if (equal(mat.get((int)p.y,(int)p.x),255,255,0))
                        p2.add(0);
                    if (equal(mat.get((int)p.y,(int)p.x),0,255,0))
                        p2.add(2);
                    if (equal(mat.get((int)p.y,(int)p.x),255,0,0))
                        p2.add(3);
                }

                l.addNeighbor(p1,p2);
            }
        links=clearLinks(links);
        ArrayList<ArrayList<Point>> shapes2=new ArrayList<ArrayList<Point>>();
        for (Link l:links)
            if (l.isValid())
                if (l.matches()<1.5) {
                    int indA1=-1, indA2=-1, indB1=-1, indB2=-1;

                    for (ArrayList<Point> v1:shapes)
                        if (v1.contains(l.getA())) {
                            indA1=shapes.indexOf(v1);
                            break;
                        }

                    for (ArrayList<Point> v1:shapes)
                        if (v1.contains(l.getB())) {
                            indB1=shapes.indexOf(v1);
                            break;
                        }

                    if (indA1==-1)
                        for (ArrayList<Point> v1:shapes2)
                            if (v1.contains(l.getA())) {
                                indA2=shapes2.indexOf(v1);
                                break;
                            }

                    if (indB1==-1)
                        for (ArrayList<Point> v1:shapes2)
                            if (v1.contains(l.getB())) {
                                indB2=shapes2.indexOf(v1);
                                break;
                            }

                    if (indA1!=-1) {
                        if (indB1!=-1) { // 1 + 1
                            if (indA1!=indB1) {
                                ArrayList<Point> shape=new ArrayList<Point>();
                                shape.addAll(shapes.get(indA1));
                                shape.addAll(shapes.get(indB1));
                                shapes2.add(shape);
                                if (indA1<indB1) {
                                    shapes.remove(indA1);
                                    shapes.remove(indB1-1);
                                }
                                else {
                                    shapes.remove(indB1);
                                    shapes.remove(indA1-1);
                                }

                            }
                        }
                        else if (indB2!=-1) { // 1 + 0
                            shapes2.get(indB2).addAll(shapes.get(indA1));
                            shapes.remove(indA1);
                        }
                    }
                    else if (indA2!=-1)
                        if (indB1!=-1) { // 0 + 1
                            shapes2.get(indA2).addAll(shapes.get(indB1));
                            shapes.remove(indB1);
                        }
                        else if (indB2!=-1) // 0 + 0
                            if (indA2!=indB2) {
                                shapes2.get(indA2).addAll(shapes2.get(indB2));
                                shapes2.remove(indB2);
                            }
                }

        for (int i=0; i<shapes2.size(); i++)
            for (int j=i+1; j<shapes2.size(); j++) {
                boolean merge=true;
                for (Point p:shapes2.get(i))
                    for (Point p2:shapes2.get(j))
                        if (p.x==p2.x&&Math.hypot(p.x-p2.x,p.y-p2.y)>20)
                            merge=false;
                if (merge==true) {
                    shapes2.get(i).addAll(shapes2.get(j));
                    shapes2.remove(j);
                    j--;
                }
            }

        ArrayList<Mat> mats=new ArrayList<Mat>();
        for (ArrayList<Point> v:shapes2) {
            Mat mat_final=new Mat(mat2.rows(),mat2.cols(),mat.type());
            Core.rectangle(mat_final,new Point(0,0),new Point(mat2.cols()-1,mat2.rows()-1),new Scalar(255,255,255),Core.FILLED);
            for (Point p:v)
                Core.line(mat_final,p,p,new Scalar(0,0,0));
            mats.add(mat_final.clone());
        }
        return mats;
    }

    private static ArrayList<Link> clearLinks(ArrayList<Link> links) {
        int size=links.size();
        for (int i=0; i<size; i++)
            if (!links.get(i).isValid()) {
                links.remove(i);
                i--;
                size=links.size();
            }
        return links;
    }

    private static ArrayList<ArrayList<Point>> isolateShapes(Mat mat2) {
        Mat mat=mat2.clone();
        ArrayList<ArrayList<Point>> shapes=new ArrayList<ArrayList<Point>>();
        for (int i=0; i<mat.width(); i++)
            for (int j=0; j<mat.height(); j++)

                if (!equal(mat.get(j,i),255,255,255)) {
                    Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED);
                    ArrayList<Point> shape=new ArrayList<Point>();
                    Queue<Point> queue=new LinkedList<Point>();
                    shape.add(new Point(i,j));
                    queue.add(new Point(i,j));

                    while (!queue.isEmpty()) {
                        Point p=queue.remove();
                        if (!shape.contains(p))
                            shape.add(p);

                        if (p.x<0||p.x>=mat.width()||p.y<0||p.y>=mat.height())
                            continue;

                        for (int a=-1; a<2; a++)
                            for (int b=-1; b<2; b++)
                                try {
                                    if (!equal(mat.get((int)p.y+b,(int)p.x+a),255,255,255)) {
                                        queue.add(new Point(p.x+a,p.y+b));
                                        Core.rectangle(mat,new Point(p.x+a,p.y+b),new Point(p.x+a,p.y+b),new Scalar(255,255,255),Core.FILLED);
                                    }
                                }
                                catch (Exception e) {
                                }
                    }
                    shapes.add(shape);
                }
        return shapes;
    }

    private static boolean equal(double[] v,int a,int b,int c) {
        return v[0]==a&&v[1]==b&&v[2]==c;
    }

    private static Mat thinning(Mat mat) {
        // Sobel x i Sobel y
        Mat mat_gray=new Mat();
        Imgproc.cvtColor(mat,mat_gray,Imgproc.COLOR_RGB2GRAY);
        return ZhangSuenThinning.process(mat_gray);
    }

    private static ArrayList<Point> curveToFunction(Mat mat) {
        ArrayList<Point> points=new ArrayList<Point>();
        int last=-1;
        for (int i=0; i<mat.width(); i++) {
            int low=-1, high=-1;
            for (int j=0; j<mat.height(); j++)
                if (mat.get(j,i)[0]==0) {
                    low=j;
                    if (high==-1)
                        high=j;
                }
            if (last==-1) {
                if (low==-1)
                    continue;
                last=low;
            }
            else if (low==-1&&high==-1)
                continue;
            else if (low<=last)
                last=low;
            else if (high>=last)
                last=high;
            else
                continue;
            points.add(new Point(i,last));
        }
        return points;
    }

    private static Mat removeScale(Mat mat,PlotModel plotModel) {
        int top=0, bottom=0, left=0, right=0;

        int[] his=new int[10];

        // Top
        for (int j=0; j<10; j++) { // create histogram
            int sum=0;
            for (int i=0; i<mat.width(); i++)
                if (mat.get(j,i)[0]==0)
                    sum++;
            his[j]=sum;
        }
        boolean non_zero=false;
        for (int i=0; i<10; i++) { // find end of scale
            if (his[i]!=0)
                non_zero=true;
            if (his[i]==0&&non_zero) {
                top=i;
                break;
            }
        }
        Core.rectangle(mat,new Point(0,0),new Point(mat.width()-1,top),new Scalar(255,255,255),Core.FILLED); // remove rectangle

        //Bottom
        non_zero=false;
        for (int j=mat.height()-1; j>mat.height()-11; j--) {
            int sum=0;
            for (int i=0; i<mat.width(); i++)
                if (mat.get(j,i)[0]==0)
                    sum++;
            his[mat.height()-1-j]=sum;
        }
        for (int i=0; i<10; i++) { // find end of scale
            if (his[i]!=0)
                non_zero=true;
            if (his[i]==0&&non_zero) {
                bottom=i;
                break;
            }
        }
        Core.rectangle(mat,new Point(0,mat.height()-1-bottom),new Point(mat.width()-1,mat.height()-1),new Scalar(255,255,255),Core.FILLED);

        //Left
        non_zero=false;
        for (int i=0; i<10; i++) {
            int sum=0;
            for (int j=0; j<mat.height(); j++) // create histogram
                if (mat.get(j,i)[0]==0)
                    sum++;
            his[i]=sum;
        }
        for (int i=0; i<10; i++) { // find end of scale
            if (his[i]!=0)
                non_zero=true;
            if (his[i]==0&&non_zero) {
                left=i;
                break;
            }
        }
        Core.rectangle(mat,new Point(0,0),new Point(left,mat.height()-1),new Scalar(255,255,255),Core.FILLED); // remove rectangle

        //Right
        non_zero=false;
        for (int i=mat.width()-1; i>mat.width()-11; i--) {
            int sum=0;
            for (int j=0; j<mat.height(); j++) // create histogram
                if (mat.get(j,i)[0]==0)
                    sum++;
            his[mat.width()-1-i]=sum;
        }
        for (int i=0; i<10; i++) { // find end of scale
            if (his[i]!=0)
                non_zero=true;
            if (his[i]==0&&non_zero) {
                right=i;
                break;
            }
        }
        Core.rectangle(mat,new Point(mat.width()-1-right,0),new Point(mat.width()-1,mat.height()-1),new Scalar(255,255,255),Core.FILLED);
        return mat;
    }

    private static void saveMat(Mat mat,String s) {
        Highgui.imwrite("resources/"+s+".png",mat);
    }

    private static Mat printPoints(ArrayList<Point> points,int w,int h,int t) {
        Mat mat=new Mat(h,w,t);
        Core.rectangle(mat,new Point(0,0),new Point(w-1,h-1),new Scalar(255,255,255),Core.FILLED);
        for (Point p:points)
            Core.line(mat,p,p,new Scalar(0,0,0));
        return mat;
    }

    private static int countPoints(Mat mat) {
        int sum=0;
        for (int i=0; i<mat.width(); i++)
            for (int j=0; j<mat.height(); j++)
                if (mat.get(j,i)[0]==0)
                    sum++;
        return sum;

    }

    private static class Link {

        private Point pointA, pointB;
        ArrayList<Integer> neighborsA, neighborsB;
        private int distance, indexA, indexB;
        boolean valid=true;

        public Link(Point _a,Point _b) {
            pointA=_a;
            pointB=_b;
            distance=(int)Math.hypot(pointB.x-pointA.x,pointB.y-pointA.y);
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean v) {
            valid=v;
        }

        public void addNeighbor(ArrayList<Integer> points,ArrayList<Integer> points2) {
            neighborsA=points;
            neighborsB=points2;
        }

        public double matches() {
            double it=0;
            int size=0;
            if (neighborsA.size()<neighborsB.size()) {
                for (Integer i:neighborsA)
                    it+=Math.abs(i-neighborsB.get(neighborsA.indexOf(i)));
                size=neighborsA.size();
            }
            else {
                for (Integer i:neighborsB)
                    it+=Math.abs(i-neighborsA.get(neighborsB.indexOf(i)));
                size=neighborsB.size();
            }
            return it/size;
        }

        public Link(Point _a,Point _b,int ia,int ib) {
            pointA=_a;
            pointB=_b;
            distance=(int)Math.hypot(pointB.x-pointA.x,pointB.y-pointA.y);
            indexA=ia;
            indexB=ib;
        }

        public int shapeA() {
            return indexA;
        }

        public int shapeB() {
            return indexB;
        }

        public Point getA() {
            return pointA;
        }

        public Point getB() {
            return pointB;
        }

        public int contains(Point x) {
            if (pointA.equals(x))
                return 0;
            if (pointB.equals(x))
                return 1;
            return -1;
        }

        public int getDistance() {
            return distance;
        }

        public boolean close(Link l) {
            return distance<l.distance;
        }

        @Override
        public String toString() {
            return "("+indexA+"<->"+indexB+") ("+pointA.x+", "+pointA.y+") <-> ("+pointB.x+", "+pointB.y+")";
        }
    }

}
