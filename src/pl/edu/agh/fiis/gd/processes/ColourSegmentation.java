package pl.edu.agh.fiis.gd.processes;

import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class ColourSegmentation {

    public static ArrayList<Double> process(Mat image) {
        Mat hsvImage = new Mat();
        ArrayList<Double> peaks = new ArrayList<>();
        Imgproc.cvtColor(image, hsvImage, Imgproc.COLOR_RGB2HSV);

        for (int j = 0; j < image.width(); j++) {
            for (int i = 0; i < image.height(); i++) {
                if (image.get(i, j)[0] > 240 && image.get(i, j)[1] > 240 && image.get(i, j)[2] > 240) { // light noise removal
                    Core.rectangle(image, new Point(j, i), new Point(j, i), new Scalar(255, 255, 255), Core.FILLED);
                    Core.rectangle(hsvImage, new Point(j, i), new Point(j, i), new Scalar(255, 255, 255), Core.FILLED);
                }
                if (image.get(i, j)[0] < 15 && image.get(i, j)[1] < 15 && image.get(i, j)[2] < 15) { // dark noise removal
                    Core.rectangle(image, new Point(j, i), new Point(j, i), new Scalar(0, 0, 0), Core.FILLED);
                    Core.rectangle(hsvImage, new Point(j, i), new Point(j, i), new Scalar(0, 0, 0), Core.FILLED);
                }
            }
        }

        List<Mat> hsv_planes = new ArrayList<Mat>();
        Core.split(hsvImage, hsv_planes);

        MatOfInt histSize = new MatOfInt(256);

        final MatOfFloat histRange = new MatOfFloat(0f, 256f);

        Mat h_hist = new Mat();

        Imgproc.calcHist(hsv_planes, new MatOfInt(0), new Mat(),
                h_hist, histSize, histRange);

        //DEBUG - draw histogram
        int hist_w = 512;
        int hist_h = 600;
        long bin_w = Math.round((double) hist_w / 256);
        Mat histImage = new Mat(hist_h, hist_w, CvType.CV_8UC1);
        for (int i = 0; i < 255; ++i) {
            Point p1 = new Point(bin_w * (i - 1), hist_h - Math.round(h_hist.get(i - 1, 0)[0]));
            Point p2 = new Point(bin_w * (i), hist_h - Math.round(h_hist.get(i, 0)[0]));
            Core.line(histImage, p1, p2, new Scalar(255, 0, 0), 2, 8, 0);
        }

        Core.bitwise_not(histImage, histImage);
        Highgui.imwrite("resources/histogram.png", histImage);
        //DEBUG - END

        //reading histogram
        int sum = 0;
        for (int i = 0; i < 255; ++i) {
            sum += h_hist.get(i, 0)[0];
        }

        int mean = sum / 256;

        int min = 0, max = -1;

        for (int i = 0; i < 255; ++i) {
            if (h_hist.get(i, 0)[0] > mean) {
                if (min == 0) {
                    min = i;
                }
                max = i;
            } else {
                if (max !=-1) {
                    int mmax = Integer.MIN_VALUE;
                    int index = 0;
                    for (int j = min; j <= max; j++) {
                        if (h_hist.get(j, 0)[0] > mmax) {
                            mmax = (int) h_hist.get(j, 0)[0];
                            index = j;
                        }
                    }
                    peaks.add((double) index);
                }
                min = 0;
                max = -1;
            }
        }

        return peaks;
    }

}
