/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.gd.processes;

import java.util.Arrays;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 *
 * @author reynev
 */
public class ThinningK3MProcess{
    
    private static boolean[][] boolA = new boolean[6][256];
    private static boolean[] boolA1pix = new boolean[256];
    private static int srcW, srcH;
    
    private static final byte WHITE = -1;
    private static final byte BLACK = 0;
    
    public static void process(Mat sourceImage, Mat resultImage) {
        srcW = sourceImage.width();
        srcH = sourceImage.height();
        byte[] imgArr = new byte[(int)sourceImage.total()];
        byte[] imgToCompare = new byte[(int)sourceImage.total()];
        prepareBoolArrays();
        
        sourceImage.get(0, 0, imgArr);
        boolean changes = true;
        
        while(changes){
            imgToCompare = Arrays.copyOf(imgArr, (int)sourceImage.total());

            for (int x = 0; x < srcW; x++) {
                for (int y = 0; y < srcH; y++) {
                    if(imgArr[y*srcW+x] == BLACK && checkWithArray(imgArr, x, y,boolA[0])){
                        imgArr[y*srcW+x] = 2;
                    }
                }
            }

            for(int i = 1 ; i < 6 ; ++i){
                for (int x = 0; x < srcW; x++) {
                    for (int y = 0; y < srcH; y++) {
                        if(imgArr[y*srcW+x] == 2 && checkWithArray(imgArr, x, y,boolA[i])){
                            imgArr[y*srcW+x] = WHITE;
                        }
                    }
                }
            }

            for (int x = 0; x < srcW; x++) {
                for (int y = 0; y < srcH; y++) {
                    if(imgArr[y*srcW+x] == 2 ){
                        imgArr[y*srcW+x] = BLACK;
                    }
                }
            }

            changes = checkChanges(imgArr, imgToCompare);
        }
        

        for (int x = 0; x < srcW; x++) {
            for (int y = 0; y < srcH; y++) {
                if(imgArr[y*srcW+x] == 1 && checkWithArray(imgArr, x, y,boolA1pix)){
//                    imageOut.setIntColor(x, y, 0);
                    imgArr[y*srcW+x] = WHITE;
                }
            }
        }
        resultImage.create(srcH, srcW, CvType.CV_8U);

        resultImage.put(0, 0, imgArr);
    }
    
    private static boolean checkWithArray(byte[] img, int x, int y, boolean[] boolArray){
        int weight = 0;
        for(int i = -1 ; i <=1 ; ++i){
            for(int j = -1 ; j <= 1 ; ++j){
                if(isValidPoint(x+i,y+j) && img[(y+j)*srcW+(x+i)] != WHITE){
                    weight += checkerMatrix[i+1][j+1];
                }
            }
        }        
        return boolArray[weight];
    }
    
    private static boolean isValidPoint(int x, int y){
        return x >= 0 && x < srcW && y >= 0 && y < srcH;
    }
    
    private static boolean checkChanges(byte[] img1, byte[] img2){
        for (int x = 0; x < srcW; x++) {
            for (int y = 0; y < srcH; y++) {
                if(img1[y*srcW+x] != img2[y*srcW+x]){
                    return true;
                }
            }
        }
        return false;
    }
    
    private static void prepareBoolArrays(){
        for(int x : A1pix){
            boolA1pix[x] = true;
        }
        for(int i = 0 ; i < 5 ; ++i){
            for(int x : A[i]){
                boolA[i][x] = true;
            }
        }
    }
    
    //static final variables
    
    private static final int[][] checkerMatrix = {{128, 64, 32}, {1, 0, 16},
                                            {2, 4, 8}};
    
    private static final int [] A1pix = {3, 6, 7, 12, 14, 15, 24, 28, 30, 31,
        48, 56, 60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135, 143, 159,
        191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243,
        247, 248, 249, 251, 252, 253, 254}; 
    
    private static final int [][] A = {
            {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60, 62, 63, 96, 112, 120,
                124, 126, 127, 129, 131, 135,143, 159, 191, 192, 193, 195, 199, 207,
                223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252,
                253, 254},
            {7, 14, 28, 56, 112, 131, 193, 224},
            {7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135, 193, 195, 224, 225, 240},
            {7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120, 124, 131, 135, 143, 193, 195,
                199, 224, 225, 227, 240, 241, 248},
            {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143,
                159, 193, 195, 199, 207, 224, 225, 227, 231, 240, 241, 243, 248, 249, 252},
            {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143,
                159, 191, 193, 195, 199, 207, 224, 225, 227, 231, 239, 240, 241, 243,
                248, 249, 251, 252, 254}
            }; 
    
}
