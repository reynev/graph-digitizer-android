package pl.edu.agh.fiis.gd.processes;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.model.PlotModel;
import pl.edu.agh.fiis.utils.ImageManager;
import pl.edu.agh.fiis.utils.Line;

/**
 *
 * @author Drobny
 */
public class TicksDetectorProcess extends AbstractProcess {
    
    
    public static void process(PlotModel plotModel) {
        Mat debugImg = plotModel.getSourceImage().clone();
        
        Mat sourceImage = plotModel.getPreprocessedImage();
        int srcW, srcH;
        srcW = sourceImage.width();
        srcH = sourceImage.height();
        Mat processTmpImage = sourceImage;// new Mat(srcH, srcW, CvType.CV_8U);
        
        Rect plotAreaRect = plotModel.getAreaRect();
        /** calculate axises ambient half height/width to look for ticks lines **/
        int xAxisAmbientHeight2 = (int) (0.08 * plotAreaRect.height);
        int yAxisAmbientWidth2 = (int) (0.08 * plotAreaRect.width);
        /** calculate ambient height in case an axis is so close to the image's edge that it would overcross it
         * normally it's 2*ambientSize **/
        int xAxisAmbientHeight = (plotAreaRect.y + plotAreaRect.height + xAxisAmbientHeight2 > srcH) ? srcH - (plotAreaRect.y + plotAreaRect.height) : 2*xAxisAmbientHeight2;
        int yAxisAmbientWidth = (plotAreaRect.x - yAxisAmbientWidth2 < 0) ? 0 : 2*yAxisAmbientWidth2;
        /** get x axis ambient from image, +/-2 pixels offset from y axis**/
        Rect xAxisAmbientRect = new Rect(plotAreaRect.x + 2, plotAreaRect.y + plotAreaRect.height - xAxisAmbientHeight2, plotAreaRect.width - 4, xAxisAmbientHeight);
        Rect yAxisAmbientRect = new Rect(plotAreaRect.x - yAxisAmbientWidth2, plotAreaRect.y + 2, yAxisAmbientWidth, plotAreaRect.height - 4);
        Mat xAxisAmbient = processTmpImage.submat(xAxisAmbientRect);
        Mat yAxisAmbient = processTmpImage.submat(yAxisAmbientRect);
        Mat xAxisTicksLines = new Mat();
        Mat yAxisTicksLines = new Mat();
        
//        System.out.println("x: "+ xAxisAmbient.width() +"x"+ xAxisAmbient.height());
//        System.out.println("y: "+ yAxisAmbient.width() +"x"+ yAxisAmbient.height());
        /** detect ticks lines **/
        Imgproc.HoughLinesP(xAxisAmbient, xAxisTicksLines, 0.5, Math.PI/360, 2, 2, 3);
        Imgproc.HoughLinesP(yAxisAmbient, yAxisTicksLines, 0.5, Math.PI/360, 2, 2, 3);
        ImageManager.saveImageMat("resources/xAxisAmbient.png", xAxisAmbient);
        ImageManager.saveImageMat("resources/yAxisAmbient.png", yAxisAmbient);
        
        Point p1,p2;
        List<Line> xTicksLines, yTicksLines;
        List<Point> xTicksList = new ArrayList();
        List<Point> yTicksList = new ArrayList();
        
        xTicksLines = filterTicks(xAxisTicksLines, xAxisAmbientHeight2, "x");
        for(Line line : xTicksLines) {
            //DEBUG
            p1 = new Point(line.x0 + xAxisAmbientRect.x, line.y0 + xAxisAmbientRect.y);
            p2 = new Point(line.x1 + xAxisAmbientRect.x, line.y1 + xAxisAmbientRect.y);
            Core.line(debugImg, p1, p2, new Scalar(0, 0, 255), 1);
            
            p1 = new Point(line.x0 + 2 + plotAreaRect.x, 0 + plotAreaRect.y + plotAreaRect.height);
            Core.circle(debugImg, p1, 2, new Scalar(0, 255, 0));
            //END
            /** point related to plot area **/
            p1 = new Point(line.x0 + 2, 0);
            xTicksList.add(p1);
        }
       
        yTicksLines = filterTicks(yAxisTicksLines, yAxisAmbientWidth2, "y");
        for(Line line : yTicksLines) {
            //DEBUG
            p1 = new Point(line.x0 + yAxisAmbientRect.x, line.y0 + yAxisAmbientRect.y);
            p2 = new Point(line.x1 + yAxisAmbientRect.x, line.y1 + yAxisAmbientRect.y);
            Core.line(debugImg, p1, p2, new Scalar(255, 0, 255), 1);
            
            p1 = new Point(0 - 1 + plotAreaRect.x, line.y0 + 2 + plotAreaRect.y);
            Core.circle(debugImg, p1, 2, new Scalar(255, 0, 0));
            //END
            /** point related to plot area **/
            p1 = new Point(0 - 1, line.y0 + 2);
            yTicksList.add(p1);
        }
        
        //Debug
        ImageManager.saveImageMat("resources/ticks.png", debugImg);
        
        plotModel.setxyTicksList(xTicksList, yTicksList);
        
        checkEdges(plotModel);
    }
    
    public static void checkEdges(PlotModel plotModel) {
        Mat debugImg = plotModel.getSourceImage().clone();
        Mat processImg = plotModel.getSourceImage().clone();
        
        Rect plotAreaRect = plotModel.getAreaRect();
        Point p00 = new Point(plotAreaRect.x, plotAreaRect.y + plotAreaRect.height);
        Point pxMax = new Point(plotAreaRect.x + plotAreaRect.width, plotAreaRect.y + plotAreaRect.height);
        Point pyMax = new Point(plotAreaRect.x, plotAreaRect.y);
        
        Point tl = new Point(plotAreaRect.x-2,plotAreaRect.y-2);
        Point br = new Point(plotAreaRect.x + plotAreaRect.width+2,
                plotAreaRect.y + plotAreaRect.height + 2);
        
        Core.rectangle(processImg, tl, br, new Scalar(255, 255, 255), Core.FILLED);
        
        Core.circle(debugImg, p00, 2, new Scalar(0, 0, 255));
        Core.circle(debugImg, pxMax, 2, new Scalar(0, 255, 0));
        Core.circle(debugImg, pyMax, 2, new Scalar(255, 0, 0));
        
        DescriptionDetector scaleDetector = new DescriptionDetector(processImg);
        Rect xMinRect = scaleDetector.detectScaleDescription(p00, DescriptionDetector.SCALE_X);
        Rect xMaxRect = scaleDetector.detectScaleDescription(pxMax, DescriptionDetector.SCALE_X);
        Rect yMinRect = scaleDetector.detectScaleDescription(p00, DescriptionDetector.SCALE_Y);
        Rect yMaxRect = scaleDetector.detectScaleDescription(pyMax, DescriptionDetector.SCALE_Y);
        
        List<Point> xTicksList = plotModel.getxTicksList();
        List<Point> yTicksList = plotModel.getyTicksList();
        
        if(xMinRect != null){
            Point xMinP = new Point(0, plotAreaRect.height);
            xTicksList.add(xMinP);
            Core.rectangle(debugImg, xMinRect.tl(), xMinRect.br(), new Scalar(0, 0, 255), 2);
        }
        if(xMaxRect != null){
            Point xMaxP = new Point(plotAreaRect.width, plotAreaRect.height);
            xTicksList.add(xMaxP);
            Core.rectangle(debugImg, xMaxRect.tl(), xMaxRect.br(), new Scalar(0, 0, 255), 2);
        }
        if(yMinRect != null){
            Point yMinP = new Point(0, plotAreaRect.height);
            yTicksList.add(yMinP);
            Core.rectangle(debugImg, yMinRect.tl(), yMinRect.br(), new Scalar(0, 0, 255), 2);
        }
        if(yMaxRect != null){
            Point yMaxP = new Point(0, 0);
            yTicksList.add(yMaxP);
            Core.rectangle(debugImg, yMaxRect.tl(), yMaxRect.br(), new Scalar(0, 0, 255), 2);
        }
        
        //Debug
        ImageManager.saveImageMat("resources/edges.png", debugImg);
        
        plotModel.setxyTicksList(xTicksList, yTicksList);
    }
    
    /**
     * Returns list of tick lines (positions related to plot area not whole image)
     * @param ticksLines
     * @param ambientSize
     * @param axis
     * @return 
     */
    private static List<Line> filterTicks(Mat ticksLines, int ambientSize, String axis) {
        double[] tick;
        double lineAngle;
        Point p1,p2, pOnAxis1,pOnAxis2;
        final int minAngle = (axis.equals("x")) ? 87 : -1 , 
                  maxAngle = (axis.equals("x")) ? 93 : 3;
        
        //DEBUG
        /** list of tick lines related to plot area **/
        List<Line> ticksList = new ArrayList();
        for(int i = 0; i < ticksLines.cols(); i++) {
            tick = ticksLines.get(0, i);
            p1 = new Point(tick[0], tick[1]);
            p2 = new Point(tick[2], tick[3]);
            lineAngle = Math.toDegrees(PlotAreaDetectorProcess.countLineAngle(p1, p2));
            /** check if line ~perpendicular to xAxis and connected with it (in 2px radius) **/
            pOnAxis1 = (axis.equals("x")) ? new Point(p1.x, ambientSize) : new Point(ambientSize, p1.y);
            pOnAxis2 = (axis.equals("x")) ? new Point(p2.x, ambientSize) : new Point(ambientSize, p2.y);
            if(lineAngle > minAngle && lineAngle < maxAngle && 
                (PlotAreaDetectorProcess.checkPointsConnection(p1, pOnAxis1, 4) ||
                PlotAreaDetectorProcess.checkPointsConnection(p2, pOnAxis2, 4)) ) {
                ticksList.add(new Line(p1, p2));
            }
        }
        return  ticksList;
    }
}
