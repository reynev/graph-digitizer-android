/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.gd.processes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.gd.model.PlotModel;

/**
 *
 * @author reynev
 */
public class ScaleConverter {

    public static void countCoefficients(PlotModel plotModel,double xMin,double xMax,double yMin,double yMax) throws GDAException {
        Rect plotAreaRect=plotModel.getAreaRect();
        List<Point> xTicksList=plotModel.getxTicksList();
        List<Point> yTicksList=plotModel.getyTicksList();
        //Line xAxis = new Line(plotAreaRect.x, plotAreaRect.y+plotAreaRect.height,
        //        plotAreaRect.x, plotAreaRect.y+plotAreaRect.height);

//        int xZero = plotAreaRect.y+plotAreaRect.height;
//        int yZero = plotAreaRect.x;
        int xListS=xTicksList.size();
        int yListS=yTicksList.size();
        double[] xLoc=new double[xListS];
        double[] yLoc=new double[yListS];
        double pxDiffX=0;
        double pxDiffY=0;
        double pxToValX;
        double pxToValY;

        if (xListS<2||yListS<2) {
//                throw new GDDException("Not enough scale ticks detected to read scale.");
            xMin=0;
            xMax=1;
            yMin=0;
            yMax=1;
            pxDiffY=plotAreaRect.height;
            pxDiffX=plotAreaRect.width;
        }
        else {
            for (int i=0; i<xListS; ++i)
                xLoc[i]=xTicksList.get(i).x;
            for (int i=0; i<yListS; ++i)
                yLoc[i]=yTicksList.get(i).y;
            Arrays.sort(xLoc);
            Arrays.sort(yLoc);

        //OLD - uses all ticks
        /*double pxXStep = countMedianStep(xLoc);
             double pxYStep = countMedianStep(yLoc);
             int xStepCount = (int)Math.round((xLoc[xListS-1] - xLoc[0])/pxXStep);
             int yStepCount = (int)Math.round((yLoc[yListS-1] - yLoc[0])/pxYStep);
             double valXStep = (xMax - xMin)/xStepCount;
             double valYStep = (yMax - yMin)/yStepCount;
             pxToValX = valXStep/pxXStep;
             pxToValY = valYStep/pxYStep;*/
            //NEW - uses only Min-Max
            pxDiffX=xLoc[xListS-1]-xLoc[0];
            pxDiffY=yLoc[yListS-1]-yLoc[0];
        }
        double valDiffX=(xMax-xMin);
        double valDiffY=(yMax-yMin);
        pxToValX=valDiffX/pxDiffX;
        pxToValY=valDiffY/pxDiffY;

//        System.err.println("XStep " + pxXStep + " xValStep " + valXStep + " stepCount " + xStepCount);
//        System.err.println("YStep " + pxYStep + " yValStep " + valYStep + " stepCount " + yStepCount);
        double zeroX;
        double zeroY;
        if (xListS<2||yListS<2) {
            zeroX=0;
            zeroY=1;
        }
        else {
            zeroX=xMin-xLoc[0]*pxToValX;
            //upperleft corner (not beginning of coordinate system)
            zeroY=plotAreaRect.height*pxToValY;
//        System.err.println("X0 " + zeroX);
//        System.err.println("Y0 " + zeroY);
        }
        plotModel.setScaleCoefficients(zeroX,zeroY,pxToValX,pxToValY);
    }

    public static Map<Integer,List<Point>> countGraphPoints(PlotModel plotModel) {
        Map<Integer,List<Point>> graphsPoints=new HashMap<>();
        Map<Integer,List<Point>> imagePoints=plotModel.getPlotValues();
        int i=0;
        for (Map.Entry<Integer,List<Point>> pointsEntry:imagePoints.entrySet())
            if (pointsEntry.getValue().size()>20) {
                List<Point> graphPoints=new ArrayList<>();
                for (Point gp:pointsEntry.getValue())
                    graphPoints.add(convertPoint(gp,plotModel));
                graphsPoints.put(i++,graphPoints);
            }
        return graphsPoints;
    }

    private static double countMedianStep(double[] locs) {
        int locsS=locs.length;
        double dst;
        double locDiffs[]=new double[locsS-1];
        for (int i=1; i<locsS; ++i) {
            dst=locs[i]-locs[i-1];
            locDiffs[i-1]=dst;
        }
        Arrays.sort(locDiffs);
        --locsS;
        double medianStep=0;
        if (locsS==1)
            medianStep=locDiffs[0];
        else if (locsS==2)
            medianStep=(locDiffs[0]+locDiffs[1])/2;
        else if (locsS%2==1)
            medianStep=locDiffs[(int)(locsS/2)+1];
        else
            medianStep=(locDiffs[(int)(locsS/2)]+locDiffs[(int)(locsS/2)+1])/2;
        return medianStep;
    }

    private static Point convertPoint(Point p,PlotModel plotModel) {
        return new Point(plotModel.getZeroX()+p.x*plotModel.getPxToValX(),
                         plotModel.getZeroY()-p.y*plotModel.getPxToValY());
    }

}
