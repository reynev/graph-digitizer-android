package pl.edu.agh.fiis.gd.processes;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.model.PlotModel;
import pl.edu.agh.fiis.utils.ImageManager;

/**
 *
 * @author Reynev
 */
public class PlotAreaDetectorProcess extends AbstractProcess {

    public static void process(PlotModel plotModel) {
        Mat sourceImage=plotModel.getPreprocessedImage();
        //Mat sourceImage = plotModel.getSourceImage();
        Rect areaRect=new Rect();
        int srcW, srcH;
        srcW=sourceImage.width();
        srcH=sourceImage.height();
        Mat processTmpImage=sourceImage;//new Mat(srcH, srcW, CvType.CV_8U);
        Mat debugImg=plotModel.getSourceImage().clone();

        System.out.println("Size: "+srcW+" x "+srcH);

        Mat lines=new Mat();
        //minimal line lenght is hardcoded, but it should be approx 0.5*min(srcW,srcH)
        System.err.println("Min dist: "+Math.min(srcW,srcH)/2);

        if (plotModel.getProcessing()) {
            Mat img=new Mat();
            Imgproc.cvtColor(debugImg,img,Imgproc.COLOR_BGR2GRAY);
            Core.bitwise_not(img,img);
//            Highgui.imwrite("grayscale.png",img);
            Imgproc.HoughLinesP(img,lines,1,Math.PI/180,1,Math.min(debugImg.width(),debugImg.height())/4,3);
        }
        else
            Imgproc.HoughLinesP(processTmpImage,lines,0.5,Math.PI/360,5,Math.min(srcW,srcH)/2,3);

//        Core.bitwise_not(processTmpImage,processTmpImage);
//        Imgproc.cvtColor(processTmpImage, processTmpImage, Imgproc.COLOR_GRAY2BGR);
//        processTmpImage.copyTo(resultImage);
        List<Point[]> axisList=detectAxis(lines,debugImg);

        System.out.println("Lines: "+lines.cols());

        /**
         * jesli 1 kandydat na osie, to nie mam problemu jesli wiecej to pytanie
         * do usera, czy dobrze wybrane lub sprawdzenie, czy dany kandydat na
         * osie ma podzialke najlepiej to i to
         */
        // t - top, b - bottom, l\r - ...
        Point tl, tr, bl, br;
        if (axisList.size()==1) {
            Point[] points=axisList.get(0);
            tl=points[0];
            bl=points[1];
            br=points[2];
            /**
             * if axis isn't perpendicular, 'tr' solution won't work it should
             * be improved to support parallelogram (hehehe, równoległobok) if
             * we have only axis it will be difficult to recognize trapeze for
             * now its enough
             */
            tr=new Point(br.x,tl.y);

            //DEBUG
            Core.line(debugImg,points[0],points[1],new Scalar(255,0,255),1);
            Core.line(debugImg,points[1],points[2],new Scalar(255,0,255),1);

            Core.line(debugImg,tl,tr,new Scalar(255,255,0),1);
            Core.line(debugImg,br,tr,new Scalar(255,255,0),1);
            //END

            areaRect=new Rect(tl,br);
        }
        else {

        }

        //Debug
//        for( int i = 0 ; i < lines.cols() ; ++i){
//            double[] iLine = lines.get(0, i);
//            Point ip1 = new Point(iLine[0], iLine[1]);
//            Point ip2 = new Point(iLine[2], iLine[3]);
//            Core.line(debugImg, ip1, ip2, new Scalar(255, 0, 0),1);
//        }
        ImageManager.saveImageMat("resources/area.png",debugImg);
        System.err.println("Area rect: "+areaRect);

        plotModel.setAreaRect(areaRect);

    }

    private static double DotProduct(Point pointA,Point pointB,Point pointC) {
        Point AB=new Point();
        Point BC=new Point();
        AB.x=pointB.x-pointA.x;
        AB.y=pointB.y-pointA.y;
        BC.x=pointC.x-pointB.x;
        BC.y=pointC.y-pointB.y;
        double dot=AB.x*BC.x+AB.y*BC.y;

        return dot;
    }

    private static double CrossProduct(Point pointA,Point pointB,Point pointC) {
        Point AB=new Point();
        Point AC=new Point();
        AB.x=pointB.x-pointA.x;
        AB.y=pointB.y-pointA.y;
        AC.x=pointC.x-pointA.x;
        AC.y=pointC.y-pointA.y;
        double cross=AB.x*AC.y-AB.y*AC.x;

        return cross;
    }

    private static double Distance(Point pointA,Point pointB) {
        return Math.hypot(pointA.x-pointB.x,pointA.y-pointB.y);
    }

    private static double LineToPointDistance2D(Point pointA,Point pointB,Point pointC) {
        double dist=CrossProduct(pointA,pointB,pointC)/Distance(pointA,pointB);
        double dot1=DotProduct(pointA,pointB,pointC);
        if (dot1>0)
            return Distance(pointB,pointC);

        double dot2=DotProduct(pointB,pointA,pointC);
        if (dot2>0)
            return Distance(pointA,pointC);
        return Math.abs(dist);
    }

    /**
     * Method which look for axis-looking pairs of lines. Axis looking means two
     * lines, which have common point (in 3 px toleracne - maybe not necessary )
     * and this common point is in point in left bottom like |_ .
     *
     * It has to be generalized to check also intersections of lines (not only
     * on ends)
     *
     * @param lines - MAtrix with lines detected by HoughLinesP
     * @return Point array in order:
     */
    private static List< Point[]> detectAxis(Mat lines,Mat mat) {
        List< Point[]> axisList=new LinkedList<>();
        ArrayList<Point> points=new ArrayList<Point>();

        int i, j;
        double[] iLine;//, jLine;
        Point ip1, ip2;//,jp1,jp2;
        //       double iAngle, jAngle;
        for (i=0; i<lines.cols(); ++i) {
            iLine=lines.get(0,i);
            ip1=new Point(iLine[0],iLine[1]);
            ip2=new Point(iLine[2],iLine[3]);

            points.add(ip1);
            points.add(ip2);
        }

        for (i=0; i<points.size(); i++)
            for (j=i+1; j<points.size(); j++)
                if (Math.hypot(points.get(i).x-points.get(j).x,points.get(i).y-points.get(j).y)<30) {
                    points.remove(j);
                    j--;
                }
//
//        Mat matt=mat.clone();
//        for (Point p:points)
//            Core.circle(matt,p,5,new Scalar(255,0,0),5);
//        Highgui.imwrite("area_points_found_in_detector1.png",matt);
//        System.out.println(points.size());

        for (i=0; i<points.size(); i++)
            for (j=0; j<points.size(); j++)
                if (j!=i)
                    for (int k=0; k<points.size(); k++)
                        if (j!=k&&i!=k)
                            if (LineToPointDistance2D(points.get(i),points.get(j),points.get(k))<10) {
                                points.remove(k);
                                k--;
                                if (k<i)
                                    i--;
                                if (k<j)
                                    j--;
                            }

//        matt=mat.clone();
//        for (Point p:points)
//            Core.circle(matt,p,5,new Scalar(255,0,0),5);
//        Highgui.imwrite("area_points_found_in_detector2.png",matt);
//        System.out.println(points.size());

        Point Pa=new Point(), Pb=new Point(), Pc=new Point();
        double Pp=0;

        for (i=0; i<points.size(); i++)
            for (j=i+1; j<points.size(); j++)
                for (int k=j+1; k<points.size(); k++) {

                    double d=Math.hypot(points.get(i).x-points.get(j).x,points.get(i).y-points.get(j).y)+Math.hypot(points.get(k).x-points.get(j).x,points.get(k).y-points.get(j).y)+Math.hypot(points.get(i).x-points.get(k).x,points.get(i).y-points.get(k).y);
                    //double d = Math.abs((points.get(j).x - points.get(i).x)*(points.get(k).y - points.get(i).y)-(points.get(j).y - points.get(i).y)*(points.get(k).x - points.get(i).x)) * 1/2;
                    if (d>Pp) {
                        Pp=d;
                        Pa=points.get(i);
                        Pb=points.get(j);
                        Pc=points.get(k);
                    }
                }

        points.clear();
        points.add(Pa);
        points.add(Pb);
        points.add(Pc);

        Mat matt=mat.clone();
        for (Point p:points)
            Core.circle(matt,p,5,new Scalar(255,0,0),5);
        Highgui.imwrite("resources/trash/area_points_found_in_detector.png",matt);
        System.out.println(points.size());

        Point tl=null, tr=null, bl=null, br=null; //top-left, top-right ...
        for (Point p:points)
            if (points.indexOf(p)==0) {
                tl=p;
                tr=p;
                bl=p;
                br=p;
            }
            else {
                if (p.x-br.x>100||p.y-br.y>100)
                    br=p;
                if (tl.x-p.x>100||tl.y-p.y>100)
                    tl=p;
                if (p.x-tr.x>100||tr.y-p.y>100)
                    tr=p;
                if (bl.x-p.x>100||p.y-bl.y>100)
                    bl=p;
            }

        System.out.println(tl+" "+tr+" "+br+" "+bl);

        if (points.size()==3) {
            if (bl==tl)
                if (Math.abs(bl.y-br.y)<Math.abs(bl.y-tr.y))
                    tl=null;
                else
                    bl=null;

            if (br==tr)
                if (Math.abs(br.y-bl.y)<Math.abs(br.y-tl.y))
                    tr=null;
                else
                    br=null;

            if (bl==br)
                if (Math.abs(bl.x-tl.x)<Math.abs(bl.x-tr.x))
                    br=null;
                else
                    bl=null;

            if (tl==tr)
                if (Math.abs(bl.x-tl.x)<Math.abs(br.x-tl.x))
                    tr=null;
                else
                    tl=null;
        }

        // If any point is missing - fill the gap
        if (tl==null)
            tl=new Point(bl.x,tr.y);
        if (bl==null)
            bl=new Point(tl.x,br.y);
        if (br==null)
            br=new Point(tr.x,bl.y);
        if (tr==null)
            tr=new Point(br.x,tl.y);

        // now every point is present, but coordinated might not match
        tl.x=bl.x;
        br.y=bl.y;

        Point[] points2=new Point[3];
        points2[0]=tl;
        points2[1]=bl;
        points2[2]=br;

        axisList.add(points2);

//            iAngle = Math.toDegrees(countLineAngle(ip1, ip2));
//            //choose left bottom point
//            if(iAngle > 45.){
//                //SWAP
//                if(ip1.y < ip2.y){
//                    ip2 = new Point(iLine[0], iLine[1]);
//                    ip1 = new Point(iLine[2], iLine[3]);
//                }
//            }else{
//                //SWAP
//                if(ip1.x > ip2.x){
//                    ip2 = new Point(iLine[0], iLine[1]);
//                    ip1 = new Point(iLine[2], iLine[3]);
//                }
//            }
//            
//            for( j = i+1 ; j < lines.cols() ; ++j){
//                jLine = lines.get(0, j);
//                jp1 = new Point(jLine[0], jLine[1]);
//                jp2 = new Point(jLine[2], jLine[3]);
//                Core.line(matt,jp1,jp2,new Scalar(0,0,255));
//                jAngle = Math.toDegrees(countLineAngle(jp1, jp2));
//                //choose left bottom point
//                if(jAngle > 45.){
//                    //SWAP
//                    if(jp2.y < jp2.y){
//                        jp2 = new Point(jLine[0], jLine[1]);
//                        jp1 = new Point(jLine[2], jLine[3]);
//                    }
//                }else{
//                    //SWAP
//                    if(jp2.x > jp2.x){
//                        jp2 = new Point(jLine[0], jLine[1]);
//                        jp1 = new Point(jLine[2], jLine[3]);
//                    }
//                }
//                
//                double angleBetween = Math.abs(jAngle-iAngle);
//                if(checkPointsConnection(ip1, jp1, 3) && angleBetween > 70 && angleBetween < 110){
//                    Point[] points = new Point[3];
//                    if(iAngle > 45){
//                        points[0] = ip2;
//                        points[1] = ip1;
//                        points[2] = jp2;
//                    }else{
//                        points[0] = jp2;
//                        points[1] = ip1;
//                        points[2] = ip2;                       
//                    }
//                    axisList.add(points);
//                    //System.out.println("Axis:  " + points);
//                }
//            }
//        }
        System.out.println("Axis:  "+axisList.size());
        return axisList;
    }

    public static double countLineAngle(Point p1,Point p2) {
        return Math.atan2(Math.abs(p2.y-p1.y),Math.abs(p2.x-p1.x));
    }

    public static boolean checkPointsConnection(Point ip,Point jp,int r) {
        //Point ip1 = new Point(iLine[0], iLine[1]);
        //Point jp2 = new Point(jLine[2], jLine[3]);

        if (Math.abs(ip.x-jp.x)<r&&Math.abs(ip.y-jp.y)<r)
            return true;

        return false;
    }

    private static void debugLines(Mat resultImage,Mat lines) {
        for (int x=0; x<lines.cols(); ++x) {
            double[] vec=lines.get(0,x);
            Point start=new Point(vec[0],vec[1]);
            Point end=new Point(vec[2],vec[3]);
            //System.err.println(x + ": " + start + " " + end);
            Core.line(resultImage,start,end,new Scalar(0,255,0),1);
        }
    }
}
