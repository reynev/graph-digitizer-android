/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.gd.processes;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.utils.ImageProcessesUtils;

/**
 * Class containing method to finding scale description on image
 * @author reynev
 */
public class DescriptionDetector {
    
    public static final int SCALE_X = 1;
    public static final int SCALE_Y = 2;
    private static final int MAX_DIFF = 5;
    private List<Rect> boundingRects;
    
    public DescriptionDetector(Mat image){
        findBoundingRects(image,true);
    }
    
    public Rect detectScaleDescription(Point tick, int scaleType){
        
        Rect boundingRect = null;
        int dst = Integer.MAX_VALUE;
        int tmpDst;
        for(Rect r : boundingRects){
            if(scaleType == SCALE_X){
                if(r.y > tick.y ){
                    tmpDst = (int) Math.abs(tick.x - (r.x + r.width/2));
                    if(tmpDst < MAX_DIFF && tmpDst < dst){
                        dst = tmpDst;
                        boundingRect = r;
                    }
                }
            }else{
                if(r.x < tick.x ){
                    tmpDst = (int) Math.abs(tick.y - (r.y + r.height/2));
                    if(tmpDst < MAX_DIFF && tmpDst < dst){
                        dst = tmpDst;
                        boundingRect = r;
                    }
                }
            }
        }
//        System.err.println("Min distance: " + dst);
        
        return boundingRect;
    }
    
    public void findBoundingRects(Mat image){
        findBoundingRects(image, true);
    }
    
    public void findBoundingRects(Mat image, boolean preprocess){
        boundingRects = new ArrayList<>();
        Mat processImage = image.clone();
        
        ImageProcessesUtils.removeColors(processImage,10);
        Imgproc.cvtColor(processImage, processImage, Imgproc.COLOR_RGB2GRAY);
        //Imgproc.GaussianBlur(processImage, processImage, new Size(5, 5), 0.7);
        Imgproc.adaptiveThreshold(processImage, processImage, 255, 
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 75, 10);
        Core.bitwise_not(processImage,processImage);
        
        Imgproc.dilate(processImage, processImage,Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, new Size(3,3)));
        Imgproc.dilate(processImage, processImage,Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, new Size(3,3)));
        
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(processImage, contours, hierarchy, Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE);
        
        MatOfPoint2f approxCurve = new MatOfPoint2f();
        MatOfPoint2f contour2f;
        MatOfPoint tmpCnt;
        
        for(MatOfPoint contour : contours){
            contour2f = new MatOfPoint2f( contour.toArray() );
            Imgproc.approxPolyDP(contour2f, approxCurve, 3, true);
            tmpCnt = new MatOfPoint( approxCurve.toArray() );
            Rect brect = Imgproc.boundingRect(tmpCnt);
            boundingRects.add(brect);
//            Core.rectangle(image, brect.tl(), brect.br(), new Scalar(255, 255, 0));
        }
        
//        ImageManager.saveImageMat("resources/comp1.png", image);
        
    }
    
    
}
