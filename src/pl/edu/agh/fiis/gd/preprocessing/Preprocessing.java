/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.fiis.gd.preprocessing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.model.PlotModel;

/**
 *
 * @author Hamish
 */
public class Preprocessing {

    public static int ACCEPTABLE_DISTANCE_VERTICAL=5;
    public static int ACCEPTABLE_DISTANCE_HORIZONTAL=5;

    public static void process(PlotModel plotModel) {
        Mat mat=plotModel.getSourceImage().clone();
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY);
//                Highgui.imwrite("nonequalized.png",mat);
        Core.normalize(mat,mat,0,255,Core.NORM_MINMAX);

//        Highgui.imwrite("equalized.png",mat);
        Mat thresholded=mat.clone();
        Imgproc.GaussianBlur(mat,mat,new Size(5,5),0.7);
        Imgproc.adaptiveThreshold(mat,thresholded,255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,Imgproc.THRESH_BINARY,11,7);
        Core.bitwise_not(thresholded,thresholded);
        Core.bitwise_not(mat,mat);
        Core.bitwise_and(mat,thresholded,mat);
        Core.bitwise_not(mat,mat);
//        Imgproc.threshold(mat,mat,180,255,Imgproc.THRESH_BINARY);
        //Highgui.imwrite("Thresholded.png",mat);

        // Deleting wrong data verticaly (texts etc)
        ArrayList<Integer> hist_vertical=new ArrayList<Integer>();
        for (int i=0; i<mat.height(); i++) {
            int sum=0;
            for (int j=0; j<mat.width(); j++)
                if (mat.get(i,j)[0]!=255)
                    sum++;
            hist_vertical.add(sum);
        }
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_GRAY2RGB);

        ArrayList<Point> empty_areas=new ArrayList<Point>();

        for (int i=0; i<hist_vertical.size(); i++)
            if (hist_vertical.get(i)==0)
                if (!empty_areas.isEmpty()) {
                    if (empty_areas.get(empty_areas.size()-1).y!=0)
                        empty_areas.add(new Point(i,0));
                }
                else
                    empty_areas.add(new Point(i,0));

            else if (!empty_areas.isEmpty())
                if (empty_areas.get(empty_areas.size()-1).y==0)
                    empty_areas.get(empty_areas.size()-1).y=i;

        Mat empty_areas_mat=mat.clone();
        for (Point p:empty_areas)
            Core.rectangle(empty_areas_mat,new Point(0,p.x),new Point(empty_areas_mat.width()-1,p.y),new Scalar(255,0,0),Core.FILLED);

        for (int i=0; i<empty_areas.size(); i++)
            if (empty_areas.get(i).y-empty_areas.get(i).x<ACCEPTABLE_DISTANCE_VERTICAL) {
                empty_areas.remove(i);
                i--;
            }
        

        for (Point p:empty_areas)
            Core.rectangle(mat,new Point(0,p.x),new Point(mat.width()-1,p.y),new Scalar(255,0,0),Core.FILLED);

        double start_index=0, stop_index=0;
        if (!empty_areas.isEmpty()) {

            if (empty_areas.get(0).x!=0) {
                System.out.println(empty_areas.get(0).x);
                start_index=0;
                stop_index=empty_areas.get(0).x;
            }

            for (int i=0; i<empty_areas.size()-1; i++)
                if (stop_index-start_index<empty_areas.get(i+1).x-empty_areas.get(i).y) {
                    start_index=empty_areas.get(i).y;
                    stop_index=empty_areas.get(i+1).x;
                }

            if (empty_areas.get(empty_areas.size()-1).y!=mat.height()-1)
                if (mat.height()-1-empty_areas.get(empty_areas.size()-1).y>stop_index-start_index) {
                    start_index=empty_areas.get(empty_areas.size()-1).y;
                    stop_index=mat.height()-1;
                }

            Core.rectangle(mat,new Point(0,0),new Point(mat.width()-1,start_index),new Scalar(255,255,255),Core.FILLED);
            Core.rectangle(mat,new Point(0,stop_index),new Point(mat.width()-1,mat.height()-1),new Scalar(255,255,255),Core.FILLED);
        }

        // Deleting wrong data horizontaly (texts etc)
        ArrayList<Integer> hist_horizontal=new ArrayList<Integer>();
        for (int i=0; i<mat.width(); i++) {
            int sum=0;
            for (int j=0; j<mat.height(); j++)
                if (mat.get(j,i)[0]!=255)
                    sum++;
            hist_horizontal.add(sum);
        }

        empty_areas=new ArrayList<Point>();

        for (int i=0; i<hist_horizontal.size(); i++)
            if (hist_horizontal.get(i)==0)
                if (!empty_areas.isEmpty()) {
                    if (empty_areas.get(empty_areas.size()-1).y!=0)
                        empty_areas.add(new Point(i,0));
                }
                else
                    empty_areas.add(new Point(i,0));

            else if (!empty_areas.isEmpty())
                if (empty_areas.get(empty_areas.size()-1).y==0)
                    empty_areas.get(empty_areas.size()-1).y=i;

//        for (Point p:empty_areas)
//            Core.rectangle(empty_areas_mat,new Point(p.x,0),new Point(p.y,empty_areas_mat.height()-1),new Scalar(0,0,255),Core.FILLED);

     //   Highgui.imwrite("empty_areas_mat1.png",empty_areas_mat);

        for (int i=0; i<empty_areas.size(); i++)
            if (empty_areas.get(i).y-empty_areas.get(i).x<ACCEPTABLE_DISTANCE_HORIZONTAL) {
                empty_areas.remove(i);
                i--;
            }

        start_index=0;
        stop_index=0;

        if (!empty_areas.isEmpty()) {
            if (empty_areas.get(0).x!=0) {
                start_index=empty_areas.get(0).x;
                stop_index=empty_areas.get(0).y;
            }

            for (int i=0; i<empty_areas.size()-1; i++)
                if (stop_index-start_index<empty_areas.get(i+1).x-empty_areas.get(i).y) {
                    start_index=empty_areas.get(i).y;
                    stop_index=empty_areas.get(i+1).x;
                }

            if (empty_areas.get(empty_areas.size()-1).y!=mat.width()-1)
                if (mat.width()-1-empty_areas.get(empty_areas.size()-1).y>stop_index-start_index) {
                    start_index=empty_areas.get(empty_areas.size()-1).y;
                    stop_index=mat.width()-1;
                }

            Core.rectangle(mat,new Point(0,0),new Point(start_index,mat.height()-1),new Scalar(255,255,255),Core.FILLED);
            Core.rectangle(mat,new Point(stop_index,0),new Point(mat.width()-1,mat.height()-1),new Scalar(255,255,255),Core.FILLED);
        }
    //    Highgui.imwrite("mat3.png",mat);
        Mat mat2=mat.clone();
        Imgproc.threshold(mat,mat2,254,255,Imgproc.THRESH_BINARY);

//        Imgproc.cvtColor(mat2,mat2,Imgproc.COLOR_BGR2GRAY);
//     //   ThinningK3MProcess.process(mat2,mat2);
//        Imgproc.cvtColor(mat2,mat2,Imgproc.COLOR_GRAY2BGR);
        ArrayList<ArrayList<Point>> shapes=isolateShapes(mat2);
        for (ArrayList<Point> shape:shapes)
            if (shape.size()<200)
                for (Point p:shape)
                    Core.line(mat2,p,p,new Scalar(255,255,255));

//        Highgui.imwrite("mat2.png",mat2);
        Imgproc.cvtColor(mat2,mat2,Imgproc.COLOR_BGR2GRAY);
        Mat lines=new Mat();
        //minimal line lenght is hardcoded, but it should be approx 0.5*min(srcW,srcH)
        Core.bitwise_not(mat2,mat2);
        Imgproc.HoughLinesP(mat2,lines,1,Math.PI/180,1,Math.min(mat2.width(),mat2.height())/4,20);

//        Imgproc.cvtColor(processTmpImage, processTmpImage, Imgproc.COLOR_GRAY2BGR);
//        processTmpImage.copyTo(resultImage);
        Mat result=detectAxis(lines,mat);
        plotModel.setSourceImage(result);
        plotModel.computePreprocessedImage();
      //  plotModel=new PlotModel(result);

//        Highgui.imwrite("preprocessed.png",result);
        //return plotModel;
    }

    private static Mat detectAxis(Mat lines,Mat mat) {
        ArrayList<Point> points=new ArrayList<Point>();

        int i, j;
        double[] iLine;//, jLine;
        Point ip1, ip2;//,jp1,jp2;
        //       double iAngle, jAngle;
        for (i=0; i<lines.cols(); ++i) {
            iLine=lines.get(0,i);
            ip1=new Point(iLine[0],iLine[1]);
            ip2=new Point(iLine[2],iLine[3]);
            points.add(ip1);
            points.add(ip2);
        }

        for (i=0; i<points.size(); i++)
            for (j=i+1; j<points.size(); j++)
                if (Math.hypot(points.get(i).x-points.get(j).x,points.get(i).y-points.get(j).y)<30) {
                    points.remove(j);
                    j--;
                }

//        Mat matt2=mat.clone();
//        for (Point p:points)
//            Core.circle(matt2,p,5,new Scalar(255,0,0),5);
       // Highgui.imwrite("area_points_found.png",matt2);

        for (i=0; i<points.size(); i++)
            for (j=0; j<points.size(); j++)
                if (j!=i)
                    for (int k=0; k<points.size(); k++)
                        if (j!=k&&i!=k)
                            if (LineToPointDistance2D(points.get(i),points.get(j),points.get(k))<10) {
                                points.remove(k);
                                if (k<i)
                                    i--;
                                if (k<j)
                                    j--;
                                k--;
                            }

        Point Pa=new Point(),Pb=new Point(),Pc=new Point();
        double Pp=0;
        
        for(i=0;i<points.size();i++){
            for(j=i+1;j<points.size();j++){
                for(int k=j+1;k<points.size();k++){      
                    
                    double d = Math.hypot(points.get(i).x - points.get(j).x,points.get(i).y - points.get(j).y) + Math.hypot(points.get(k).x - points.get(j).x,points.get(k).y - points.get(j).y) + Math.hypot(points.get(i).x - points.get(k).x,points.get(i).y - points.get(k).y);
                    //double d = Math.abs((points.get(j).x - points.get(i).x)*(points.get(k).y - points.get(i).y)-(points.get(j).y - points.get(i).y)*(points.get(k).x - points.get(i).x)) * 1/2;
                    if(d>Pp){
                        Pp = d;
                        Pa = points.get(i);
                        Pb = points.get(j);
                        Pc = points.get(k);
                    }
                }
            }
        }
        
        points.clear();
        points.add(Pa);
        points.add(Pb);
        points.add(Pc);
        
        Mat matt3=mat.clone();
        for (Point p:points)
            Core.circle(matt3,p,5,new Scalar(255,0,0),5);
        Highgui.imwrite("resources/trash/area_points_found.png",matt3);

        Point tl=null, tr=null, bl=null, br=null; //top-left, top-right ...
        for (Point p:points)
            if (points.indexOf(p)==0) {
                tl=p;
                tr=p;
                bl=p;
                br=p;
            }
            else {
                if (p.x-br.x>100||p.y-br.y>100)
                    br=p;
                if (tl.x-p.x>100||tl.y-p.y>100)
                    tl=p;
                if (p.x-tr.x>100||tr.y-p.y>100)
                    tr=p;
                if (bl.x-p.x>100||p.y-bl.y>100)
                    bl=p;
            }

        if (points.size()==3) {
            if (bl==tl)
                if (Math.abs(bl.y-br.y)<Math.abs(bl.y-tr.y))
                    tl=null;
                else
                    bl=null;

            if (br==tr)
                if (Math.abs(br.y-bl.y)<Math.abs(br.y-tl.y))
                    tr=null;
                else
                    br=null;

            if (bl==br)
                if (Math.abs(bl.x-tl.x)<Math.abs(bl.x-tr.x))
                    br=null;
                else
                    bl=null;

            if (tl==tr)
                if (Math.abs(bl.x-tl.x)<Math.abs(br.x-tl.x))
                    tr=null;
                else
                    tl=null;
        }

        // If any point is missing - fill the gap
        if (tl==null)
            tl=new Point(bl.x,tr.y);
        if (bl==null)
            bl=new Point(tl.x,br.y);
        if (br==null)
            br=new Point(tr.x,bl.y);
        if (tr==null)
            tr=new Point(br.x,tl.y);

        // now every point is present, but coordinated might not match
//        tl.x=bl.x;
//        br.y=bl.y;
        Point[] points2=new Point[3];
        points2[0]=tl;
        points2[1]=bl;
        points2[2]=br;

        tr=new Point(br.x-bl.x+tl.x,br.y-(bl.y-tl.y));

        Point S=new Point(bl.x+(tr.x-bl.x)/2,tr.y+(bl.y-tr.y)/2);

//        double arc=(Math.atan((br.y-bl.y)/(br.x-bl.x)) * (180/Math.PI));
//        Mat matt=rotate(mat,arc);
//        System.out.println((br.y-bl.y)+" "+(br.x-bl.x)+" "+arc);
        double wsp=150;

        Point P=tl;

        double wspol_a=S.x-P.x;
        double wspol_b=S.y-P.y;
        double wspol_c=Math.hypot(wspol_a,wspol_b);

        double wspol_a_prim=wspol_a/wspol_c*(wspol_c+wsp);
        double wspol_b_prim=wspol_b/wspol_c*(wspol_c+wsp);

        tl=new Point(S.x-wspol_a_prim,S.y-wspol_b_prim);
        br=new Point(S.x+wspol_a_prim,S.y+wspol_b_prim);

        P=tr;

        wspol_a=S.x-P.x;
        wspol_b=S.y-P.y;
        wspol_c=Math.hypot(wspol_a,wspol_b);

        wspol_a_prim=wspol_a/wspol_c*(wspol_c+wsp);
        wspol_b_prim=wspol_b/wspol_c*(wspol_c+wsp);

        tr=new Point(S.x-wspol_a_prim,S.y-wspol_b_prim);
        bl=new Point(S.x+wspol_a_prim,S.y+wspol_b_prim);

//        Core.line(mat,tr,S,new Scalar(0,0,100));
//        Core.line(mat,S,tl,new Scalar(0,0,255));
//        Core.line(mat,S,bl,new Scalar(255,0,0));
//        Core.line(mat,br,S,new Scalar(100,0,0));
//        axisList.add(points2);
        Mat pt1=new Mat(4,1,CvType.CV_32FC2);
        pt1.put(0,0,
                (float)tl.x,(float)tl.y,
                (float)tr.x,(float)tr.y,
                (float)br.x,(float)br.y,
                (float)bl.x,(float)bl.y);

        Mat pt2=new Mat(4,1,CvType.CV_32FC2);
        pt2.put(0,0,
                0.0,0.0,
                Math.hypot(bl.x-br.x,bl.y-br.y),0,
                Math.hypot(bl.x-br.x,bl.y-br.y),Math.hypot(bl.x-tl.x,bl.y-tl.y),
                0,Math.hypot(bl.x-tl.x,bl.y-tl.y));

        Mat transform=Imgproc.getPerspectiveTransform(pt1,pt2);
        Mat mat2=mat.clone();

        Imgproc.warpPerspective(mat2,mat2,transform,new Size(Math.hypot(bl.x-br.x,bl.y-br.y),Math.hypot(bl.x-tl.x,bl.y-tl.y)));

        Mat mat3=new Mat(mat2.rows()+2,mat2.cols()+2,CvType.CV_8UC1);
        Imgproc.floodFill(mat2,mat3,new Point(0,0),new Scalar(255,255,255),null,new Scalar(0,0,0),new Scalar(254,254,254),Imgproc.FLOODFILL_FIXED_RANGE);
        mat3=new Mat(mat2.rows()+2,mat2.cols()+2,CvType.CV_8UC1);
        Imgproc.floodFill(mat2,mat3,new Point(0,mat2.height()-1),new Scalar(255,255,255),null,new Scalar(0,0,0),new Scalar(254,254,254),Imgproc.FLOODFILL_FIXED_RANGE);
        mat3=new Mat(mat2.rows()+2,mat2.cols()+2,CvType.CV_8UC1);
        Imgproc.floodFill(mat2,mat3,new Point(mat2.width()-1,0),new Scalar(255,255,255),null,new Scalar(0,0,0),new Scalar(254,254,254),Imgproc.FLOODFILL_FIXED_RANGE);
        mat3=new Mat(mat2.rows()+2,mat2.cols()+2,CvType.CV_8UC1);
        Imgproc.floodFill(mat2,mat3,new Point(mat2.width()-1,mat2.height()-1),new Scalar(255,255,255),null,new Scalar(0,0,0),new Scalar(254,254,254),Imgproc.FLOODFILL_FIXED_RANGE);

//        Highgui.imwrite("mat3.png",mat3);
//        Highgui.imwrite("mat2.png",mat2);
        return mat2;
    }

    private static Mat rotate(Mat src,double angle) {
        Mat dst=new Mat();
        int len=Math.max(src.cols(),src.rows());
        Point pt=new Point(len/2.,len/2.);
        Mat r=Imgproc.getRotationMatrix2D(pt,angle,1.0);
        Imgproc.warpAffine(src,dst,r,new Size(len,len));
        return dst;
    }

    private static ArrayList<ArrayList<Point>> isolateShapes(Mat mat2) {
        Mat mat=mat2.clone();
        ArrayList<ArrayList<Point>> shapes=new ArrayList<ArrayList<Point>>();
        for (int i=0; i<mat.width(); i++)
            for (int j=0; j<mat.height(); j++)

                if (!equal(mat.get(j,i),255,255,255)) {
                    Core.rectangle(mat,new Point(i,j),new Point(i,j),new Scalar(255,255,255),Core.FILLED);
                    ArrayList<Point> shape=new ArrayList<Point>();
                    Queue<Point> queue=new LinkedList<Point>();
                    shape.add(new Point(i,j));
                    queue.add(new Point(i,j));

                    while (!queue.isEmpty()) {
                        Point p=queue.remove();
                        if (!shape.contains(p))
                            shape.add(p);

                        if (p.x<0||p.x>=mat.width()||p.y<0||p.y>=mat.height())
                            continue;

                        for (int a=-1; a<2; a++)
                            for (int b=-1; b<2; b++)
                                try {
                                    if (!equal(mat.get((int)p.y+b,(int)p.x+a),255,255,255)) {
                                        queue.add(new Point(p.x+a,p.y+b));
                                        Core.rectangle(mat,new Point(p.x+a,p.y+b),new Point(p.x+a,p.y+b),new Scalar(255,255,255),Core.FILLED);
                                    }
                                }
                                catch (Exception e) {
                                }
                    }
                    shapes.add(shape);
                }
        return shapes;
    }

    private static boolean equal(double[] v,int a,int b,int c) {
        return v[0]==a&&v[1]==b&&v[2]==c;
    }

    private static double DotProduct(Point pointA,Point pointB,Point pointC) {
        Point AB=new Point();
        Point BC=new Point();
        AB.x=pointB.x-pointA.x;
        AB.y=pointB.y-pointA.y;
        BC.x=pointC.x-pointB.x;
        BC.y=pointC.y-pointB.y;
        double dot=AB.x*BC.x+AB.y*BC.y;

        return dot;
    }

    private static double CrossProduct(Point pointA,Point pointB,Point pointC) {
        Point AB=new Point();
        Point AC=new Point();
        AB.x=pointB.x-pointA.x;
        AB.y=pointB.y-pointA.y;
        AC.x=pointC.x-pointA.x;
        AC.y=pointC.y-pointA.y;
        double cross=AB.x*AC.y-AB.y*AC.x;

        return cross;
    }

    private static double Distance(Point pointA,Point pointB) {
        return Math.hypot(pointA.x-pointB.x,pointA.y-pointB.y);
    }

    private static double LineToPointDistance2D(Point pointA,Point pointB,Point pointC) {
        double dist=CrossProduct(pointA,pointB,pointC)/Distance(pointA,pointB);
        double dot1=DotProduct(pointA,pointB,pointC);
        if (dot1>0)
            return Distance(pointB,pointC);

        double dot2=DotProduct(pointB,pointA,pointC);
        if (dot2>0)
            return Distance(pointA,pointC);
        return Math.abs(dist);
    }
}
