/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.gd.exception;

/**
 * Exception class for informing about exceptions in gdd.
 * @author reynev
 */
public class GDAException extends Exception{
    
    public GDAException(String msg){
        super(msg);
    }
}
