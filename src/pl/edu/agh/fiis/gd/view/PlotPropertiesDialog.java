package pl.edu.agh.fiis.gd.view;

import java.util.LinkedHashMap;

import pl.edu.agh.fiis.gd.ActivityView;
import pl.edu.agh.fiis.gd.GraphDigitizerController;
import pl.edu.agh.fiis.gd.R;
import pl.edu.agh.fiis.gd.model.PlotModel;
import pl.edu.agh.fiis.gd.model.PlotModel.PlotType;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class PlotPropertiesDialog extends ScrollView {
	
    private LinkedHashMap<ComponentType, EditText> componentsMap = new LinkedHashMap<>();
    private CheckBox cbHasLegend;
    private RadioButton radioLine;
    private RadioButton radioPoint;
    private PlotModel plotModel;
    private ActivityView activityView;
    private LinearLayout mainLayout;
    
    public static enum ComponentType{TITLE, X_TITLE, Y_TITLE,
        X_MIN, X_MAX, Y_MIN, Y_MAX   }
    
	public PlotPropertiesDialog(Context context, PlotModel pm, ActivityView aV) {
		super(context);
		mainLayout = new LinearLayout(context);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        addView(mainLayout,params);
        addPropertiesField(context, "Plot title", ComponentType.TITLE, pm.getTitle());
        addPropertiesField(context, "X axis title", ComponentType.X_TITLE, pm.getxTitle());
        addPropertiesField(context, "X min", ComponentType.X_MIN, pm.getxMin());
        addPropertiesField(context, "X max", ComponentType.X_MAX, pm.getxMax());
        addPropertiesField(context, "Y axis title", ComponentType.Y_TITLE, pm.getyTitle());
        addPropertiesField(context, "Y min", ComponentType.Y_MIN, pm.getyMin());
        addPropertiesField(context, "Y max", ComponentType.Y_MAX, pm.getyMax());
//        addCheckBox(context, "Has legend", pm.isHasLegend());
        addRadioButtons(context, "Line Graph", "Point graph", pm.getType());
        
        plotModel = pm;
        activityView = aV;
	}
	
	private void addPropertiesField(Context c, String label, final ComponentType type, String content) {
		LinearLayout actualRow = new LinearLayout(c);
        actualRow.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actualRow.setLayoutParams(params);
        mainLayout.addView(actualRow);
        
    	TextView textView = new TextView(c);
        textView.setText(label);
        params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        actualRow.addView(textView);

    	EditText editText = new EditText(c);
        editText.setText(content);
        componentsMap.put(type, editText);
        
        ImageButton editBtn = new ImageButton(c);
        params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        editBtn.setImageResource(R.drawable.ic_action_edit);
        editBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				activityView.editDescriptionRectangle(type);
			}
		});
        actualRow.addView(editBtn,params);
        
        params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        actualRow.addView(editText,params);
        
    }
	
	private void addCheckBox(Context c, String label, boolean checked) {
		LinearLayout actualRow = new LinearLayout(c);
        actualRow.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actualRow.setLayoutParams(params);
        mainLayout.addView(actualRow);
        
        cbHasLegend = new CheckBox(c);
        cbHasLegend.setText(label);
        cbHasLegend.setChecked(checked);
        params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        cbHasLegend.setLayoutParams(params);
        actualRow.addView(cbHasLegend);
	}
	
	private void addRadioButtons(Context c, String labelLine, String labelPoint, PlotModel.PlotType type) {
		LinearLayout actualRow = new LinearLayout(c);
        actualRow.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actualRow.setLayoutParams(params);
        mainLayout.addView(actualRow);
        
        RadioGroup rg = new RadioGroup(c);
        params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        actualRow.addView(rg,params);
        
        radioLine = new RadioButton(c);
        radioLine.setText(labelLine);
        params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        rg.addView(radioLine,params);
        
        radioPoint = new RadioButton(c);
        radioPoint.setText(labelPoint);
        params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        rg.addView(radioPoint,params);

        if(type ==PlotType.TYPE_LINE){
        	radioLine.setChecked(true);
        }else{
        	radioPoint.setChecked(true);        	
        }
	}

	public void applyValues() {
		EditText editText;
		for(LinkedHashMap.Entry<ComponentType, EditText>  entry : componentsMap.entrySet()){
			editText = entry.getValue();
			switch (entry.getKey()) {
			case TITLE:
				plotModel.setTitle(editText.getText().toString());
				break;
			case X_TITLE:
				plotModel.setxTitle(editText.getText().toString());
				break;
			case Y_TITLE:
				plotModel.setyTitle(editText.getText().toString());
				break;
			case X_MIN:
				plotModel.setxMin(editText.getText().toString());
				break;
			case X_MAX:
				plotModel.setxMax(editText.getText().toString());
				break;
			case Y_MIN:
				plotModel.setyMin(editText.getText().toString());
				break;
			case Y_MAX:
				plotModel.setyMax(editText.getText().toString());
				break;
			default:
				break;
			}
		}
		
		//plotModel.setHasLegend(cbHasLegend.isChecked());
		if(radioLine.isChecked()){
			plotModel.setType(PlotType.TYPE_LINE);
		}else{
			plotModel.setType(PlotType.TYPE_POINT);
		}
		
	}

}
