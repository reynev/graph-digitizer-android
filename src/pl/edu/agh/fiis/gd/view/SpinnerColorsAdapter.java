package pl.edu.agh.fiis.gd.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerColorsAdapter extends BaseAdapter {

	Map<Integer, Integer> colors;
    Context context;

    public SpinnerColorsAdapter(Context context, Map<Integer, Integer> colorsInPlots) 
    {
        this.context=context;
        colors = colorsInPlots;
    }
    @Override
    public int getCount() 
    {
        return colors.size();
    }
    @Override
    public Object getItem(int arg0) 
    {
        return colors.get(arg0);
    }
    @Override
    public long getItemId(int arg0) 
    {
        return arg0;
    }
    @Override
    public View getView(int pos, View view, ViewGroup parent){
        LayoutInflater inflater=LayoutInflater.from(context);
        view=inflater.inflate(android.R.layout.simple_spinner_dropdown_item, null);
        TextView txv=(TextView)view.findViewById(android.R.id.text1);
        //float[] hsv = {colors.get(pos)/255, 1, 0.7f};
        System.err.println("Spinner color: " + pos + " col: " + colors.get(pos));
        txv.setBackgroundColor(colors.get(pos));
        txv.setTextSize(20f);
        txv.setText("------- Plot " + pos );
        return view;
    }

}
