package pl.edu.agh.fiis.gd.view;

import android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class RectView extends View {

    private Paint dynamicPaint;
    private Paint staticPaint;
    
    private Rect staticRect;

    private int mStartX = 0;
    private int mStartY = 0;
    private int mEndX = 0;
    private int mEndY = 0;
    private boolean mDrawRect = false;
    private boolean drawingEnabled = false;

    private OnUpCallback mCallback = null;
    
    public static enum ActionType { SELECT_LEGEND, SELECT_TEXT};
    
    private ActionType actionType;
    
	double displayRatio;
	int xTranslation;

    public interface OnUpCallback {
        void onRectFinished(Rect rect);
    }

    public RectView(final Context context) {
        super(context);
        init();
    }

    public RectView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RectView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Sets callback for up
     *
     * @param callback {@link OnUpCallback}
     */
    public void setOnUpCallback(OnUpCallback callback) {
        mCallback = callback;
    }

    /**
     * Inits internal data
     */
    private void init() {
        dynamicPaint = new Paint();
        dynamicPaint.setColor(getContext().getResources().getColor(R.color.holo_red_light));
        dynamicPaint.setStyle(Paint.Style.STROKE);
        dynamicPaint.setStrokeWidth(5);
        
        staticPaint = new Paint();
        staticPaint.setColor(getContext().getResources().getColor(R.color.holo_green_light));
        staticPaint.setStyle(Paint.Style.STROKE);
        staticPaint.setStrokeWidth(5);

    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {

        // TODO: be aware of multi-touches
    	if(drawingEnabled){
	        switch (event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	                mDrawRect = false;
	                mStartX = (int) event.getX();
	                mStartY = (int) event.getY();
	                invalidate();
	                break;
	
	            case MotionEvent.ACTION_MOVE:
	                final int x = (int) event.getX();
	                final int y = (int) event.getY();
	
	                if (!mDrawRect || Math.abs(x - mEndX) > 5 || Math.abs(y - mEndY) > 5) {
	                    mEndX = x;
	                    mEndY = y;
	                    invalidate();
	                }
	
	                mDrawRect = true;
	                break;
	
	            case MotionEvent.ACTION_UP:
	                if (mCallback != null) {
	                    mCallback.onRectFinished(new Rect(Math.min(mStartX, mEndX), Math.min(mStartY, mEndY),
	                            Math.max(mEndX, mStartX), Math.max(mEndY, mStartX)));
	                }
	                invalidate();
	                break;
	
	            default:
	                break;
	        }
    	}

        return true;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawRect) {
            canvas.drawRect(Math.min(mStartX, mEndX), Math.min(mStartY, mEndY),
                    Math.max(mEndX, mStartX), Math.max(mEndY, mStartY), dynamicPaint);
        }
        
        if(staticRect != null){
        	canvas.drawRect(staticRect, staticPaint);
        }
    }
    
    public void setStacicRect(Rect rect){
    	if(rect != null){
	    	staticRect = rect;
	    	staticRect = new Rect((int)(xTranslation+rect.left/displayRatio),
	    			(int)(rect.top/displayRatio), 
	    			(int)(xTranslation+rect.right/displayRatio),
	    			(int)(rect.bottom/displayRatio));
	    	mDrawRect = false;
	    	invalidate();
    	}
    	mEndX = 0;
    	mEndY = 0;
    	mStartX = 0;
    	mStartY = 0;
    }
    
    public Rect getSelectedRect(){
    	Rect rect = null;
    	if(mStartX != 0 || mStartY != 0 || mEndX != 0 || mEndY != 0){
	    	rect = new Rect((int)((Math.min(mStartX, mEndX)- xTranslation)*displayRatio ),
	    			(int)(Math.min(mStartY, mEndY)*displayRatio),
	    			(int)((Math.max(mEndX, mStartX)- xTranslation)*displayRatio),
	                (int)(Math.max(mEndY, mStartY)*displayRatio));
    	}
    	return rect;
    }
    
    public void disableSelecting(){
    	staticRect = null;
    	drawingEnabled = false;
    	mDrawRect = false;
    	invalidate();
    }

	public void enableDrawing(boolean enableDrawing, ActionType type) {
		this.drawingEnabled = enableDrawing;
		actionType = type;
	}
	
	public void countDisplayRatio(int plotW, int plotH){
    	double oh = plotH;
    	double dH = this.getHeight();
    	displayRatio = oh/dH;

    	double ow = plotW;
    	double screenW = this.getWidth();
    	double dW = ow / displayRatio;
    	
    	xTranslation = (int) ((screenW-dW)/2);
    	
//    	System.err.println("Ratio " + displayRatio + " xTrans " + xTranslation);
    }

	public ActionType getActionType() {
		return actionType;
	}
	
}
