/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.agh.fiis.gd.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.utils.ImageProcessesUtils;

/**
 *
 * @author reynev
 */
public class PlotModel {
	
	public static enum PlotType {TYPE_LINE, TYPE_POINT};

    /**
     * Original plot image
     */
    private Mat sourceImage;
    private Mat preprocessedImage;

    private Map<Integer,List<Point>> plotValues;
    private Rect areaRect;
    private double zeroX;
    private double zeroY;
    private double pxToValX;
    private double pxToValY;
    private boolean legendReaded = false;
    private boolean processing = false;
    
    private String title;
    private String xTitle;
    private String yTitle;
    private String xMin = "0";
    private String xMax = "1";
    private String yMin = "0";
    private String yMax = "1";
    private boolean hasLegend = false;
    private PlotType type = PlotType.TYPE_LINE;
    
    private List<Point> xTicksList, yTicksList;
    /**
     * Map of plot colors in graph. plot_number-color
     */
    private Map<Integer,Integer> plotsInLegend=new HashMap<Integer,Integer>();

    public PlotModel(Mat srcImg){
        sourceImage = srcImg;
        computePreprocessedImage();
    }
    
    public void computePreprocessedImage(){
    	preprocessedImage = ImageProcessesUtils.preparePreprocessedImage(sourceImage);
    }

    public boolean getProcessing(){
        return processing;
    }
    
    public void setProcessing(boolean bool){
        processing=bool;
    }
    
    public Mat getSourceImage() {
        return sourceImage;
    }

    public void setAreaRect(Rect areaRect) {
        this.areaRect=areaRect;
    }

    public Rect getAreaRect() {
        return areaRect;
    }
    
    public List<Point> getxTicksList() {
        return xTicksList;
    }

    public Map<Integer,Integer> getPlotsInLegend() {
        return plotsInLegend;
    }

    public void setPlotsInLegend(Map<Integer,Integer> plotsInLegend) {
        this.plotsInLegend=plotsInLegend;
    }

    public void setxyTicksList(List<Point> xTicksList,List<Point> yTicksList) {
        this.xTicksList=xTicksList;
        this.yTicksList=yTicksList;
    }

    public List<Point> getyTicksList() {
        return yTicksList;
    }

    public Map<Integer,List<Point>> getPlotValues() {
        return plotValues;
    }

    public void setScaleCoefficients(double zeroX,double zeroY,double pxToValX,double pxToValY) {
        this.zeroX=zeroX;
        this.zeroY=zeroY;
        this.pxToValX=pxToValX;
        this.pxToValY=pxToValY;
    }

    public double getZeroX() {
        return zeroX;
    }

    public double getZeroY() {
        return zeroY;
    }

    public double getPxToValX() {
        return pxToValX;
    }

    public double getPxToValY() {
        return pxToValY;
    }

    public void setPlotValues(Map<Integer,List<Point>> plotValues) {
        this.plotValues=plotValues;
    }

    public boolean isLegendReaded() {
        return legendReaded;
    }

    public void setLegendReaded(boolean legendReaded) {
        this.legendReaded = legendReaded;
    }
    
    public Mat getPreprocessedImage() {
        return preprocessedImage;
    }

    public void setPreprocessedImage(Mat preprocessedImage) {
        this.preprocessedImage = preprocessedImage;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getxTitle() {
		return xTitle;
	}

	public void setxTitle(String xTitle) {
		this.xTitle = xTitle;
	}

	public String getyTitle() {
		return yTitle;
	}

	public void setyTitle(String yTitle) {
		this.yTitle = yTitle;
	}

	public String getxMin() {
		return xMin;
	}

	public void setxMin(String xMin) {
		this.xMin = xMin;
	}

	public String getxMax() {
		return xMax;
	}

	public void setxMax(String xMax) {
		this.xMax = xMax;
	}

	public String getyMin() {
		return yMin;
	}

	public void setyMin(String yMin) {
		this.yMin = yMin;
	}

	public String getyMax() {
		return yMax;
	}

	public void setyMax(String yMax) {
		this.yMax = yMax;
	}

	public boolean isHasLegend() {
		return hasLegend;
	}

	public void setHasLegend(boolean hasLegend) {
		this.hasLegend = hasLegend;
	}

	public PlotType getType() {
		return type;
	}

	public void setType(PlotType type) {
		this.type = type;
	}
	
	public void setSourceImage(Mat mat){
		sourceImage = mat;
    }
    
}
