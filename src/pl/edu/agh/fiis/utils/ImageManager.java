/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.utils;

import java.io.File;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import android.graphics.Bitmap;
import android.os.Environment;

/**
 * Loads static image to OpenCV Mat object Converts Mat to BufferedImage
 *
 * @author Drobny
 */
public class ImageManager {

    public ImageManager() {
    }

    public static Mat loadImageMat(String fileName) {
        return Highgui.imread(fileName);
    }

    public static void saveImageMat(String fileName, Mat img) {
    	File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = new File(storageDir.getAbsoluteFile() + "/"+ fileName);
        Highgui.imwrite(imageFile.getAbsolutePath(), img);
    }

    /**
     * Converts image in Mat to BufferedImage
     *
     * @param matIn
     * @return BufferedImage
     */
    public static Bitmap mat2Img(Mat matIn) {
    	Bitmap bmpOut;
        int width, height;
        width = matIn.width();
        height = matIn.height();

        byte[] data = new byte[width * height * (int) matIn.elemSize()];
        matIn.get(0, 0, data);

        bmpOut = Bitmap.createBitmap(matIn.cols(), matIn.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(matIn, bmpOut);

        return bmpOut;
    }
    
}
