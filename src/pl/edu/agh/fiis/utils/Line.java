package pl.edu.agh.fiis.utils;

import org.opencv.core.Point;

public class Line {
        public double x0,y0,x1,y1;
        public Point p1,p2;
        
        public Line(Point p1, Point p2) {
            this.x0 = p1.x;
            this.y0 = p1.y;
            this.x1 = p2.x;
            this.y1 = p2.y;
            this.p1 = new Point(p1.x, p1.y);
            this.p2 = new Point(p2.x, p2.y);
        }
        
        public Line(double x0, double y0, double x1, double y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
            this.p1 = new Point(x0, y0);
            this.p2 = new Point(x1, y1);
        }
    }