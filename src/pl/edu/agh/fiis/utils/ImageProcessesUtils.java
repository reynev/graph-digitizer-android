/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.agh.fiis.utils;

import java.io.FileNotFoundException;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import pl.edu.agh.fiis.gd.exception.GDAException;
import pl.edu.agh.fiis.gd.processes.ThinningK3MProcess;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

/**
 *
 * @author reynev
 */
public class ImageProcessesUtils {
    
    /**
     * Method that removes all colors and leaves only black and gray.
     * @param srcImg
     * @return 
     */
    public static void removeColors(Mat srcImg, int maxDiff){
        
        double r,bdiff, gdiff;
        for(int i =0;i<srcImg.width();i++){
            for(int j=0;j<srcImg.height();j++){
                r = srcImg.get(j,i)[0];
                bdiff = srcImg.get(j,i)[1] - r;
                gdiff = srcImg.get(j,i)[2] - r;
		if( bdiff > maxDiff || bdiff < -maxDiff || gdiff > maxDiff || gdiff < -maxDiff ){
                    Core.line(srcImg,new Point(i,j),new Point(i,j), new Scalar(255,255,255));
		}
            }
        }
        
        //ImageManager.saveImageMat("resources/mask.png", srcImg);
        
    }
    
    public static Mat preparePreprocessedImage(Mat img){
        Mat preprocessedImage = img.clone();
        ImageProcessesUtils.removeColors(preprocessedImage,10);
        
        Imgproc.cvtColor(preprocessedImage, preprocessedImage, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(preprocessedImage, preprocessedImage, new Size(5, 5), 0.7);
        Imgproc.adaptiveThreshold(preprocessedImage, preprocessedImage, 255, 
                Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 75, 10);
        
        ThinningK3MProcess.process(preprocessedImage,preprocessedImage);
        //preprocessedImage = ZhangSuenThinning.process(preprocessedImage);
        
        //Debug
        ImageManager.saveImageMat("resources/mask.png", preprocessedImage);
        
        Core.bitwise_not(preprocessedImage,preprocessedImage);
        return preprocessedImage;
    }
    
    public static Bitmap scalePictureToImageView( Uri photoPAth, ContentResolver cr, int targetW, int targetH) throws FileNotFoundException {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(cr.openInputStream(photoPAth), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeStream(cr.openInputStream(photoPAth), null, bmOptions);
    }
    
}
