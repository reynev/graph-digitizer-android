package pl.edu.agh.fiis.ocr;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ocr.FeaturesVector;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.content.Context;

/**
 *
 * @author Drobny
 */
public class OcrProcess {
    public final static int HORIZONTAL = 0;
    public final static int VERTICAL = 1;
    
    final static int trainingImageSize = 18;
    
    private final List<FeaturesVector> trainingVectorsAll;
    private final List<FeaturesVector> trainingVectorsNumbers;
    private final kNN knn;
    
    public OcrProcess(Context activityContext) {
        Training training = new Training(activityContext);
        trainingVectorsAll = training.trainingVectorsAll;
        trainingVectorsNumbers = training.trainingVectorsNumbers;
        
        knn = new kNN(5);
    }
    
    public String process(Mat rectImage, String type, final int orientation) {
        List<String> sings = new ArrayList<>();
        
        Mat tmpRectImage;
        List<MatOfPoint> contours = new ArrayList();
        Rect tmpRect;
        Mark tmpMark;
        String tmpSign;
        
        List<FeaturesVector> trainingVectors;
        if(type.equals("number")) {
            trainingVectors = trainingVectorsNumbers;
        } else {
            trainingVectors = trainingVectorsAll;
        }
        
        if(rectImage.channels() > 1) {
            Imgproc.cvtColor(rectImage, rectImage, Imgproc.COLOR_BGR2GRAY);
        }
        Imgproc.adaptiveThreshold(rectImage, rectImage, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 3, 8);
        
        /** TODO - not working properly **/
        if(orientation == OcrProcess.VERTICAL) {
            Mat tmpRectImage2 = new Mat(new Size(rectImage.height(), rectImage.width()), CvType.CV_8UC1);
            rotateImage90(rectImage.clone(), tmpRectImage2);
            rectImage.release();
            rectImage = tmpRectImage2.clone();
        }
        /** DEBUG **/
//        File imageFile = new File("resources/out.png" );
//        Highgui.imwrite(imageFile.getAbsolutePath(), rectImage);
        
        tmpRectImage = new Mat(rectImage.size(), CvType.CV_8UC1);
        Core.bitwise_not(rectImage, tmpRectImage);
        
        Imgproc.findContours(tmpRectImage, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
//        System.out.println("marks on image: "+ contours.size());
        if(!contours.isEmpty()) {
            for(MatOfPoint mp : contours) {
                tmpRect = Imgproc.boundingRect(mp);
                
                tmpMark = new Mark(rectImage.submat(tmpRect));
                tmpMark.extractMark();
                
                tmpSign = knn.process(trainingVectors, tmpMark.featuresVector);
                sings.add(tmpSign);
                
            }
            Collections.reverse(sings); /** contours are from right side so reverse numbers **/
        }
        
        StringBuilder result = new StringBuilder();
        for(String sign : sings) {
            result.append(sign);
        }
//        System.out.println("OCR result: "+ result);
        
        return result.toString();
    }
    
    private void rotateImage90(Mat in, Mat out) {
        Core.transpose(in, out);
        Core.flip(out, out, 1);
    }
}
