package pl.edu.agh.fiis.ocr;

import java.util.ArrayList;
import java.util.List;

import ocr.FeaturesVector;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Mark {
    public int width, height;
    public Mat image;
    public FeaturesVector featuresVector;
    
    public String sign;
    
    public Mark(Mat image) {
        this.image = image;
        width = image.width();
        height = image.height();
    }
    public void setSign(String sign) {
        this.sign = sign;
    }
    
    /**
     * Crop image width
     */
    public void cropLetterImage() {
        Mat resultImage = new Mat(image.size(), CvType.CV_8UC1);
        List<MatOfPoint> contours = new ArrayList();
        Rect rect;
        /** Image negative for findContours **/
        Core.bitwise_not(image, resultImage);
        
        Imgproc.findContours(resultImage, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
        if(!contours.isEmpty()) {
            int biggestContour = Mark.getBiggestContourIndex(contours);
            rect = Imgproc.boundingRect(contours.get(biggestContour));

            image = image.submat(rect);
            width = image.width();
            height = image.height();
        }
    }
    /**
     * Normalize image to OcrProcess.trainingImageSize height
     * @param maxHeight
     */
    public void normalizeMarkImage(int maxHeight) {
        double factor = image.width() / (double) image.height();
        Size dsize = new Size((int) (maxHeight * factor), maxHeight);
        
        Imgproc.resize(image, image, dsize);
        width = image.width();
        height = image.height();
    }
    
    /**
     * Getting feature points from letter
     */
    public void extractMark() {
        FeaturesVector letterFeaturesVector = null;
        
        normalizeMarkImage(OcrProcess.trainingImageSize);
        
        Imgproc.adaptiveThreshold(image, image, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 5, 4);
//        Imgproc.threshold(image, image, 128, 255, Imgproc.THRESH_BINARY);
        
        MarkExtractionProcess extraction = new MarkExtractionProcess();
        List<Object> vector = extraction.process(image);

        letterFeaturesVector = new FeaturesVector(vector, false);
        double factor =  (width / ((double) height));
        letterFeaturesVector.setSizeFactor(factor);
        
        featuresVector = letterFeaturesVector;
    }
    
    public static int getBiggestContourIndex(List<MatOfPoint> contours) {
        int tmpBiggestContourSize = 0, tmpIndex = 0;
        for(int i = 0; i < contours.size(); i++) {
            if(contours.get(i).size().area() > tmpBiggestContourSize) {
                tmpIndex = i;
            }
        }
        return tmpIndex;
    }
}
