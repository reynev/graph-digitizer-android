package pl.edu.agh.fiis.ocr;

import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author Drobny
 */
public class MarkExtractionProcess {
    
    Mat image;
    int size = 3; // 3x3
    
    public List<Object> process(Mat in) {
        int width = in.width(), height = in.height(), w,h;
        w = width/size;
        h = height/size;
        
        if(w == 0) {
            List<Object> vec = new ArrayList();
            for(int i = 0; i < size * size; i++) {
                vec.add((double) h);
            }
            return vec;
        }
        
        image = in.clone();
        Core.bitwise_not(image, image); /** for findContours **/
        
        int x0,y0,x1,y1;
        List<Object> vector = new ArrayList();
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                x0 = i * w;
                y0 = j * h;
                x1 = (i == size - 1) ? width : (i * w + w);
                y1 = (j == size - 1) ? height : (j * h + h);
                vector.add((double) getPixelDensityInRect(new Rect(new Point(x0, y0), new Point(x1, y1))));
            }
        }
        return vector;
    }
    
    /**
     * @param rect
     * @return amount of pixels in specified rectangle (sum pixels amount from each contour)
     */
    private int getPixelDensityInRect(Rect rect) {
        List<MatOfPoint> contours = new ArrayList();
        Imgproc.findContours(image.submat(rect), contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
        if(contours.isEmpty()) {
            return 0;
        }
        int sum = 0;
        for(MatOfPoint m : contours) {
            sum += m.size().area();
        }
        return sum;
    }
}
