package pl.edu.agh.fiis.ocr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ocr.FeaturesVector;

public class kNN {
    int k;
    
    public kNN(int k) {
        this.k = k;
    }
    
    public String process(List<FeaturesVector> trainingVectors, FeaturesVector probeVec) {
        List<Character> distanceList = new ArrayList();
        
        double tmpDistance, sizeFactorTmpDistance;
        for(FeaturesVector trainingVec : trainingVectors) {
            tmpDistance = trainingVec.getDistance(probeVec);
            sizeFactorTmpDistance = trainingVec.getSizeFactorDistance(probeVec);
            distanceList.add(new Character(trainingVec.getClassName(), tmpDistance, sizeFactorTmpDistance));
        }
        
        Collections.sort(distanceList, new CharComparator());
        
        List<Character> nearest;
        if(k < distanceList.size()) {
            nearest = distanceList.subList(0, k);
        } else {
            nearest = distanceList;
        }
//        System.out.println(nearest);
        
        /** grouping by class Name 
         *  className , distance sum
         **/
        Map<String, Double> votes = new HashMap();
        for(Character p : nearest) {
            if(votes.containsKey(p.className)) {
                votes.put(p.className, (votes.get(p.className) + p.dist) / 2.0);
            } else {
                votes.put(p.className, p.dist);
            }
        }
        /** countig votes **/
        Map<String, Integer> votesCount = new HashMap();
        for(Character p : nearest) {
            if(votesCount.containsKey(p.className)) {
                votesCount.put(p.className, votesCount.get(p.className) + 1);
            } else {
                votesCount.put(p.className, 1);
            }
        }
        
        double distWithSizeFactor;
        for(Character p : nearest) {
            distWithSizeFactor = p.dist + p.factorDist * (0.1 * p.dist);
            votes.put(p.className, distWithSizeFactor);
        }
        
        int votesCountTmp;
        for(Character p : nearest) {
            votesCountTmp = votesCount.get(p.className);
            if(votesCountTmp > 1) {
                votes.put(p.className, votes.get(p.className) * (1/ (double) votesCountTmp) );
            } 
        }
        
        
        double minDist = Double.MAX_VALUE;
        String classNameWithMinDist = "";
        for(Map.Entry<String, Double> entry : votes.entrySet()) {
            if(entry.getValue() < minDist) {
                minDist = entry.getValue();
                classNameWithMinDist = entry.getKey();
            }
        }
        
        return classNameWithMinDist;
    }
    
    class Character {
        String className;
        double dist;
        double factorDist;
        
        public Character(String className, double value, double factor) {
            this.className = className;
            this.dist = value;
            this.factorDist = factor;
        }
        @Override
        public String toString() {
            return "["+ className +", "+ factorDist +","+ dist +"]";
        }
    }
    
    class CharComparator implements Comparator<Character> {

        @Override
        public int compare(Character o1, Character o2) {
            Double d1 = o1.dist, d2 = o2.dist;
            int c1 = d1.compareTo(d2);
            if(c1 != 0) {
                return c1;
            } else {
                d1 = o1.factorDist;
                d2 = o2.factorDist;
                return d1.compareTo(d2);
            }
        }
        
    }
}
