package pl.edu.agh.fiis.ocr;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ocr.FeaturesVector;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.util.Log;

/**
 *
 * @author Drobny
 */
public class Training {
    final String trainingDataNumbersFileName = "trainingDataNumbers.ser";
    final String trainingDataFileName = "trainingDataAll.ser";
    
    final boolean TRAINING_DATA_FROM_FILE = true;
    
    public List<FeaturesVector> trainingVectorsAll = new ArrayList();
    public List<FeaturesVector> trainingVectorsNumbers = new ArrayList();
    
    public Training(Context activityContext) {
        getDataFromFile(activityContext);
    }
    
    /** 
     * Deserialize training data from assets
     */
    private void getDataFromFile(Context activityContext) {
        try {
            ObjectInputStream in = new ObjectInputStream(activityContext.getAssets().open(trainingDataNumbersFileName));
            trainingVectorsNumbers = (List<FeaturesVector>) in.readObject();
            in.close();
            
            in = new ObjectInputStream(activityContext.getAssets().open(trainingDataFileName));
            trainingVectorsAll = (List<FeaturesVector>) in.readObject();
            in.close();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
